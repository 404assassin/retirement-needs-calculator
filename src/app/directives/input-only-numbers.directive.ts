import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[appInputOnlyNumbers]'
})
export class InputOnlyNumbersDirective {

    private navigationKeys = [
        '.',
        'Backspace',
        'Delete',
        'Tab',
        'Escape',
        'Enter',
        'Home',
        'End',
        'ArrowLeft',
        'ArrowRight',
        'Clear',
        'Copy',
        'Paste'
    ];
    inputElement: HTMLElement;

    constructor(public el: ElementRef) {
        this.inputElement = el.nativeElement;
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputOnlyNumbersDirective constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.inputElement::', this.inputElement,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    @HostListener('keydown', ['$event'])
    onKeyDown($event: KeyboardEvent) {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputOnlyNumbersDirective onKeyDown :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::$event::', $event,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        if (
            this.navigationKeys.indexOf($event.key) > -1 || // Allow: navigation keys: backspace, delete, arrows etc.
            ($event.key === 'a' && $event.ctrlKey === true) || // Allow: Ctrl+A
            ($event.key === 'c' && $event.ctrlKey === true) || // Allow: Ctrl+C
            ($event.key === 'v' && $event.ctrlKey === true) || // Allow: Ctrl+V
            ($event.key === 'x' && $event.ctrlKey === true) || // Allow: Ctrl+X
            ($event.key === 'a' && $event.metaKey === true) || // Allow: Cmd+A (Mac)
            ($event.key === 'c' && $event.metaKey === true) || // Allow: Cmd+C (Mac)
            ($event.key === 'v' && $event.metaKey === true) || // Allow: Cmd+V (Mac)
            ($event.key === 'x' && $event.metaKey === true) // Allow: Cmd+X (Mac)
        ) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if (
            ($event.shiftKey || ($event.keyCode < 48 || $event.keyCode > 57)) &&
            ($event.keyCode < 96 || $event.keyCode > 105)
        ) {
            $event.preventDefault();
        }
    }

    @HostListener('paste', ['$event'])
    onPaste(event: ClipboardEvent) {
        event.preventDefault();
        const pastedInput: string = event.clipboardData
            .getData('text/plain')
            // .replace(/\D/g, ''); // get a digit-only string
            .replace(/\d*\.?\d?/g, ''); // get a digit-only string
        document.execCommand('insertText', false, pastedInput);
    }

    @HostListener('drop', ['$event'])
    onDrop(event: DragEvent) {
        event.preventDefault();
        // const textData = event.dataTransfer.getData('text').replace(/\D/g, '');
        const textData = event.dataTransfer.getData('text').replace(/\d*\.?\d?/g, '');
        this.inputElement.focus();
        document.execCommand('insertText', false, textData);
    }

}
