import { FormGroup, ValidatorFn, ValidationErrors } from '@angular/forms';

export const LifeExpendToRetirementAgeDirective: ValidatorFn = (control: FormGroup): ValidationErrors | null => {

    const lifeExpectancy = control.get('lifeExpectancy');
    const retirementAge = control.get('retirementAge');
/*    console.log(
        '\n:::::::::::::::::::::::::::::::::::::: LifeExpendToRetirementAgeDirective :::::::::::::::::::::::::::::::::::::::::::::::::::',
        '\n::this::', this,
        '\n::currentAge::', lifeExpectancy,
        '\n::retirementAge::', retirementAge,
        '\n::lifeExpectancy.value::', lifeExpectancy.value,
        '\n::retirementAge.value::', retirementAge.value,
        '\n::parseFloat(lifeExpectancy.value) >= 120::', parseFloat(lifeExpectancy.value) >= 120,
        '\n::parseFloat(lifeExpectancy.value) < parseFloat(retirementAge.value)::', parseFloat(lifeExpectancy.value) < parseFloat(retirementAge.value),
        '\n::lifeExpectancy && retirementAge && parseFloat(lifeExpectancy.value) <= parseFloat(retirementAge.value) || parseFloat(lifeExpectancy.value) >= 120::', lifeExpectancy && retirementAge && parseFloat(lifeExpectancy.value) <= parseFloat(retirementAge.value) || parseFloat(lifeExpectancy.value) >= 120,

        '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    );*/
    return lifeExpectancy && retirementAge && parseFloat(lifeExpectancy.value) < parseFloat(retirementAge.value) || parseFloat(lifeExpectancy.value) > 120 ? {'lifeExpendToRetirementAgeValidator': true} : null;
};
