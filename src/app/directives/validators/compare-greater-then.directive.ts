import { FormGroup, ValidatorFn, ValidationErrors } from '@angular/forms';

export const CompareGreaterThenValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {

    const currentAge = control.get('currentAge');
    const retirementAge = control.get('retirementAge');
/*    console.log(
        '\n:::::::::::::::::::::::::::::::::::::: CompareGreaterThenDirective :::::::::::::::::::::::::::::::::::::::::::::::::::',
        '\n::this::', this,
        '\n::currentAge::', currentAge,
        '\n::retirementAge::', retirementAge,
        '\n::currentAge.value::', currentAge.value,
        '\n::retirementAge.value::', retirementAge.value,
        '\n::currentAge.value > retirementAge.value::', currentAge.value > retirementAge.value,
        '\n::retirementAge.value >= 80::', retirementAge.value >= 80,
        '\n::retirementAge.value >= 80 || currentAge.value > retirementAge.value::', retirementAge.value >= 80 || currentAge.value > retirementAge.value,
        '\n::currentAge && retirementAge && retirementAge.value >= 80 || currentAge.value > retirementAge.value::', currentAge && retirementAge && retirementAge.value >= 80 || currentAge.value > retirementAge.value,
        '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    );*/
    return currentAge && retirementAge && retirementAge.value > 80 || currentAge.value > retirementAge.value ? {'currentAgeGreaterThenRetirementAge': true} : null;
};

/** example */
/*
export const identityRevealedValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const name = control.get('name');
  const alterEgo = control.get('alterEgo');

  return name && alterEgo && name.value === alterEgo.value ? { 'identityRevealed': true } : null;
};*/
