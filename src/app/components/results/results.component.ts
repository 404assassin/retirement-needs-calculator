import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { RetirementCalculationsService } from '../../services/retirement-calculations.service';
import { gsap, Power2 } from "gsap/all";

interface LooseObject {
    [key: string]: any
}

@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
    @ViewChild("resultsBar1", {static: false}) private resultsBar1: ElementRef;
    @ViewChild("resultsBar2", {static: false}) private resultsBar2: ElementRef;
    public displayData: LooseObject;
    public graphMessageHead: string;
    public graphMessageBody: string;

    constructor(/*****((((!!||!!))))*****/
                private retirementCalculationsService: RetirementCalculationsService,
                /*****((((!!||!!))))*****/) {

        this.retirementCalculationsService.currentDisplayState.subscribe(message => {
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: ResultsComponent constructor retirementCalculationsService update :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::message::', message,
            //     '\n::message.displayStatus::', message.displayStatus,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );

            if ( typeof message !== 'undefined' ) {
                this.updateView(message);
            }
        });
    }

    ngOnInit() {
    }

    private updateView(displayValues: LooseObject) {
        this.displayData = displayValues;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ResultsComponent updateView :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::displayValues::', displayValues,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this.resultsBar1 !== 'undefined' ) {
            this.updateHavePerMonth(displayValues.graphMsg);
            this.updateHaveBar(displayValues.graphHaveStyles.height);
            this.updateNeedBar(displayValues.graphNeedStyles.height);
        }
    }

    private updateHavePerMonth(graphMessage: LooseObject): void {
        this.graphMessageHead = graphMessage.head;
        this.graphMessageBody = graphMessage.body;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ResultsComponent updateHavePerMonth :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

    }

    private updateHaveBar(graphHeight: string): void {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ResultsComponent updateHaveBar :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::graphHeight::', graphHeight,
        //     '\n::this.resultsBar1::', this.resultsBar1,
        //     '\n::this.resultsBar1.nativeElement::', this.resultsBar1.nativeElement,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        gsap.to(this.resultsBar1.nativeElement, 0.65, {
            autoAlpha: 1.0,
            height: graphHeight,
            transformOrigin: "50% 100%",
            ease: Power2.easeOut,
        });
    }

    private updateNeedBar(graphHeight: string): void {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ResultsComponent updateNeedBar :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::graphHeight::', graphHeight,
        //     '\n::this.resultsBar1::', this.resultsBar2,
        //     '\n::this.resultsBar1.nativeElement::', this.resultsBar2.nativeElement,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        gsap.to(this.resultsBar2.nativeElement, 0.65, {
            autoAlpha: 1.0,
            height: graphHeight,
            transformOrigin: "50% 100%",
            ease: Power2.easeOut,
        });
    }

}
