import { Component, ElementRef, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DynamicVariablesService } from '../../services/dynamic-variables.service';
import { RetirementCalculationsService } from '../../services/retirement-calculations.service';
import { CurrencyPipe } from '@angular/common';
import { CompareGreaterThenValidator } from '../../directives/validators/compare-greater-then.directive';
import { LifeExpendToRetirementAgeDirective } from '../../directives/validators/life-expend-to-retirement-age.directive';
import { RangeCheck } from '../../validators/range-check.validator';
import { MatDialog } from '@angular/material/dialog';
import { gsap, Power2 } from "gsap/all";
import { ModalComponent } from '../global/modal/modal.component';
import { Utilities } from '../../shared/utilities';

interface LooseObject {
    [key: string]: any
}

@Component({
    selector: 'app-calculator',
    templateUrl: './calculator.component.html',
    styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements AfterViewInit, OnInit {
    @ViewChild('formWrapper', {'static': true}) formWrapper: ElementRef;
    @ViewChild('currentAge', {'static': true}) currentAge: ElementRef;
    @ViewChild('retirementAge', {'static': true}) retirementAge: ElementRef;
    @ViewChild('annualEarned', {'static': true}) annualEarned: ElementRef;
    @ViewChild('savedTotal', {'static': true}) savedTotal: ElementRef;
    @ViewChild('toggleArea', {'static': true}) toggleArea: ElementRef;

    private areaToggleState: boolean;
    private dynamicValues: any;

    public perMonthReplace: number;
    public perMonthContribution: number;
    public calculatorForm = new FormGroup({
            currentAge: new FormControl(30,
                [
                    Validators.required,
                    Validators.minLength(1),
                    // this.valueGreaterThenValidator()
                ]),
            retirementAge: new FormControl(67,
                [
                    Validators.required,
                    Validators.minLength(1)
                ]
            ),
            annualEarned: new FormControl('$45,000',
                [
                    Validators.required,
                    Validators.minLength(1),
                ]
            ),
            savedTotal: new FormControl('$20,000',
                [
                    Validators.required,
                    Validators.minLength(1),
                ]
            ),
            ssIncome: new FormControl(true),
            incomePercentage: new FormControl(80,
                [
                    Validators.required,
                    RangeCheck(1, 200,),
                ]),
            lifeExpectancy: new FormControl(92,
                [
                    Validators.required,
                    RangeCheck(1, 200,),
                ]),
            averageInvestmentGrowth: new FormControl(6,
                [
                    Validators.required,
                    RangeCheck(0, 25,),
                ]),
            averagePayIncrease: new FormControl(3.8,
                [
                    Validators.required,
                    RangeCheck(0, 10,),
                ]),
            retirementPlanContribution: new FormControl(3,
                [
                    Validators.required,
                    RangeCheck(0, 100,),
                ]),
        },
        {validators: [CompareGreaterThenValidator, LifeExpendToRetirementAgeDirective]}
    );

    get validation() {
        return this.calculatorForm.controls;
    }

    constructor(/*<<<<:::::::::::::::::::::::::::::::::::::::::>>>>*/
                private currencyPipe: CurrencyPipe,
                private dynamicVariablesService: DynamicVariablesService,
                public dialog: MatDialog,
                private retirementCalculationsService: RetirementCalculationsService,
                /*<<<<:::::::::::::::::::::::::::::::::::::::::>>>>*/) {
        this.getData();
    }

    ngOnInit() {
        // this.update();
    }

    ngAfterViewInit(): void {
        // this.focusField(this.currentAge);
        this.currentAge.nativeElement.focus();
    }

    public getData(): void {
/*        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
        this.dynamicVariablesService.getDatas().subscribe(
            (data) => {
                this.dynamicValues = data;
  /*              console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent getData :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::data::', data,
                    '\n::this.dynamicValues::', this.dynamicValues,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
            },
            (error) => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  CalculatorComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => this.dataUpdate(this.dynamicValues)
        );
    }

    private dataUpdate(data): void {
        this.updateFormDynamicValues(data);
/*        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::data::', data,
            '\n::this.dynamicVariable::', this.dynamicValues,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/

    }

    private updateFormDynamicValues(data): void {
        this.calculatorForm.patchValue({
            currentAge: data.current_age_default,
            retirementAge: data.retirement_age_default,
            annualEarned: data.annual_earned_default,
            savedTotal: data.saved_total_default,
            ssIncome: data.ss_income_default,
            incomePercentage: data.income_percentage_default,
            lifeExpectancy: data.life_expectancy_default,
            averageInvestmentGrowth: data.average_investment_growth_default,
            averagePayIncrease: data.average_pay_increase_default,
            retirementPlanContribution: data.retirement_plan_contribution_default,
        });
   /*     console.log(
            '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent updateForm :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::data::', data,
            '\n::this.dynamicVariable::', this.dynamicValues,
            '\n::this.dynamicVariable.currentAge::', this.dynamicValues.currentAge,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
        this.update();
    }

    public update(): void {
/*        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent update :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.calculatorForm::', this.calculatorForm,
            '\n::this.calculatorForm.value::', this.calculatorForm.value,
            '\n::this.calculatorForm.controls.currentAge::', this.calculatorForm.controls.currentAge,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
        this.updateIncomePerMonth();
        this.updateContributionPerMonth();
        this.updateCalculationData();
    }

    openDialog() {
        const dialogRef = this.dialog.open(ModalComponent, {
            height: '622px',
            width: '94vw',
            disableClose: false
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });

    }

    private updateIncomePerMonth() {
        let annualEarned: number = parseFloat(this.calculatorForm.controls.annualEarned.value.replace(/[^0-9.-]+/g, ''));
        let incomePercent: number = this.calculatorForm.controls.incomePercentage.value;
        let perMonthEarned: number = annualEarned / 12;
        this.perMonthReplace = perMonthEarned * incomePercent / 100;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: updateIncomePerMonth :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::perMonthEarned::', perMonthEarned,
        //     '\n::perMonthReplace::', this.perMonthReplace,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    private updateContributionPerMonth() {
        let annualEarned: number = parseFloat(this.calculatorForm.controls.annualEarned.value.replace(/[^0-9.-]+/g, ''));
        let retirementPlanContribution: number = this.calculatorForm.controls.retirementPlanContribution.value;
        let perMonthEarned: number = annualEarned / 12;
        this.perMonthContribution = perMonthEarned * retirementPlanContribution / 100;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: updateIncomePerMonth :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::perMonthEarned::', perMonthEarned,
        //     '\n::perMonthContribution::', this.perMonthContribution,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    private updateCalculationData() {
        let formValues: LooseObject = {
            currentAge: this.calculatorForm.controls.currentAge.value,
            retirementAge: this.calculatorForm.controls.retirementAge.value,
            annualEarned: parseFloat(this.calculatorForm.controls.annualEarned.value.replace(/[^0-9.-]+/g, '')),
            savedTotal: parseFloat(this.calculatorForm.controls.savedTotal.value.replace(/[^0-9.-]+/g, '')),
            ssIncome: this.calculatorForm.controls.ssIncome.value,
            incomePercentage: parseFloat(this.calculatorForm.controls.incomePercentage.value),
            lifeExpectancy: parseFloat(this.calculatorForm.controls.lifeExpectancy.value),
            averageInvestmentGrowth: parseFloat(this.calculatorForm.controls.averageInvestmentGrowth.value),
            averagePayIncrease: parseFloat(this.calculatorForm.controls.averagePayIncrease.value),
            retirementPlanContribution: parseFloat(this.calculatorForm.controls.retirementPlanContribution.value),
            dataAssumption: this.dynamicValues
        };

        // valueParsedFloated = parseFloat(this.value);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent updateCalculationData :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.calculatorForm.controls.currentAge.value::', this.calculatorForm.controls.currentAge.value,
        //     '\n::this.calculatorForm.controls.retirementAge.value::', this.calculatorForm.controls.retirementAge.value,
        //     '\n::parseFloat(this.calculatorForm.controls.annualEarned.value.replace(/[^0-9.-]+/g, \'\'))::', parseFloat(this.calculatorForm.controls.annualEarned.value.replace(/[^0-9.-]+/g, '')),
        //     '\n::parseFloat(this.calculatorForm.controls.savedTotal.value.replace(/[^0-9.-]+/g, \'\'))::', parseFloat(this.calculatorForm.controls.savedTotal.value.replace(/[^0-9.-]+/g, '')),
        //     '\n::this.calculatorForm.controls.incomePercentage.value::', this.calculatorForm.controls.incomePercentage.value,
        //     '\n::this.calculatorForm.controls.ssIncome.value::', this.calculatorForm.controls.ssIncome.value,
        //     '\n::this.calculatorForm.controls.lifeExpectancy.value::', this.calculatorForm.controls.lifeExpectancy.value,
        //     '\n::this.calculatorForm.controls.averageInvestmentGrowth.value::', this.calculatorForm.controls.averageInvestmentGrowth.value,
        //     '\n::this.calculatorForm.controls.averagePayIncrease.value::', this.calculatorForm.controls.averagePayIncrease.value,
        //     '\n::this.calculatorForm.controls.retirementPlanContribution.value::', this.calculatorForm.controls.retirementPlanContribution.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.retirementCalculationsService.setCalculationValues(formValues);
    }

    public onChange($event: Event, formControlName: string): void {
        let valuePiped: any = 0;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent onChange A :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$event::', $event,
        //     '\n::formControlName::', formControlName,
        //     '\n::this.calculatorForm.value[formControlName]::', this.calculatorForm.value[formControlName],
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        valuePiped = this.currencyPipe.transform(this.calculatorForm.value[formControlName].replace(/\D/g, ''), 'USD', "symbol", '1.0-0');
        this.calculatorForm.patchValue({
            [formControlName]: valuePiped,
        });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: CalculatorComponent onChange B :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.calculatorForm.value::', this.calculatorForm.value,
        //     '\n::this.calculatorForm.value[formControlName]::', this.calculatorForm.value[formControlName],
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public focusField(formFieldId: any): void {
        this[formFieldId].nativeElement.focus();
        this[formFieldId].nativeElement.select();
    }

    public toggleAreaState(value: any): void {
        this.areaToggleState = value;
        if ( !!this.areaToggleState ) {
            gsap.to(this.toggleArea.nativeElement, 0.75, {
                autoAlpha: 1.0,
                height: "auto",
                ease: Power2.easeOut,
                onComplete: () => {
                    gsap.set(this.toggleArea.nativeElement, {
                        overflow: 'visible',
                    });
                }
            });
        } else if ( !this.areaToggleState ) {
            gsap.to(this.toggleArea.nativeElement, {
                duration: 0.95,
                autoAlpha: 0.750,
                height: 0,
                ease: Power2.easeOut,
                overflow: 'hidden',
            });
        }

    }

}
