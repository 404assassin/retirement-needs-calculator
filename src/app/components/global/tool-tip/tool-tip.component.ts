import { Component, ElementRef, Input, OnInit, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { gsap, Power2 } from "gsap/all";

@Component({
    selector: 'app-tool-tip',
    templateUrl: './tool-tip.component.html',
    styleUrls: ['./tool-tip.component.scss']
})
export class ToolTipComponent implements OnInit, AfterViewInit {
    @Input() toolTipContent: string;
    @ViewChild("toolTipDialogueWrapper", {static: false}) private toolTipDialogueWrapper: ElementRef;
    @ViewChild("toolTipIconWrapper", {static: true}) private toolTipIconWrapper: ElementRef;
    @ViewChild("toolTipContentBox", {static: false}) private toolTipContentBox: ElementRef;
    private tipState: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        let childPos = this.toolTipContentBox.nativeElement.offsetParent.offsetParent;
        let childWidth = this.toolTipContentBox.nativeElement.getBoundingClientRect().width;
        let parentWidth = this.toolTipDialogueWrapper.nativeElement.offsetParent.offsetParent.offsetParent.getBoundingClientRect().width;
        let childOffset = -childPos.offsetLeft + parentWidth / 2 - childWidth / 2;
        if ( childOffset > -2 ) {
            childOffset = -2;
        } else if ( childOffset < -(childWidth - 20) ) {
            childOffset = -(childWidth - 20);
        }

        this.positionToolTipDialog(childOffset);
        this.switchOff();
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToolTipComponent ngAfterViewInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::childPos::', childPos,
        //     '\n::childWidth::', childWidth,
        //     '\n::childWidth / 2::', childWidth / 2,
        //     '\n::parentWidth::', parentWidth,
        //     '\n::parentWidth / 2::', parentWidth / 2,
        //     '\n::childOffset::', childOffset,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    private positionToolTipDialog(childOffset: number): void {
        gsap.to(this.toolTipContentBox.nativeElement, {
            duration: 0.5,
            left: childOffset,
            ease: Power2.easeOut,
        });

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToolTipComponent positionToolTipDialog :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::childOffset::', childOffset,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public tipToggle(): void {
        this.tipState = !this.tipState;
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ToolTipComponent tipToggle :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.tipState::', this.tipState,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        if ( !!this.tipState ) {
            this.switchOn();
        } else if ( !this.tipState ) {
            this.switchOff();
        }
    }

    public switchOn(): void {
        let childPos = this.toolTipContentBox.nativeElement.offsetParent.offsetParent;
        let childWidth = this.toolTipContentBox.nativeElement.getBoundingClientRect().width;
        let parentWidth = this.toolTipDialogueWrapper.nativeElement.offsetParent.offsetParent.offsetParent.getBoundingClientRect().width;
        let childOffset = -childPos.offsetLeft + parentWidth / 2 - childWidth / 2;
        // this.tipState = true;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToolTipComponent switchOn :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.tipState::', this.tipState,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        gsap.to(this.toolTipDialogueWrapper.nativeElement, 0.35, {
            autoAlpha: 1.0,
            scale: 1,
            left: childOffset,
            marginTop: '20px',
            ease: Power2.easeOut,
        });

    }

    public switchOff(): void {
        // this.tipState = false;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToolTipComponent switchOff :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.tipState::', this.tipState,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        gsap.to(this.toolTipDialogueWrapper.nativeElement, 0.5, {
            autoAlpha: 0.0,
            scale: 0,
            marginTop: '-25px',
            // transformOrigin: "-50% 100%",
            ease: Power2.easeOut,
        });
    }
}

