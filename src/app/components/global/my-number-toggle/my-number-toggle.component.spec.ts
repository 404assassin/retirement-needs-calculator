import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyNumberToggleComponent } from './my-number-toggle.component';

describe('MyNumberToggleComponent', () => {
  let component: MyNumberToggleComponent;
  let fixture: ComponentFixture<MyNumberToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyNumberToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyNumberToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
