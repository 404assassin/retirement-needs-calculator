import { Component, AfterViewInit, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { gsap, Power2 } from "gsap/all";

@Component({
    selector: 'app-my-number-toggle',
    templateUrl: './my-number-toggle.component.html',
    styleUrls: ['./my-number-toggle.component.scss']
})
export class MyNumberToggleComponent implements AfterViewInit, OnInit {
    @ViewChild("symbolMinus", {static: true}) private symbolMinus: ElementRef;
    @ViewChild("symbolPlus", {static: true}) private symbolPlus: ElementRef;

    @Output() toggleState: EventEmitter<any> = new EventEmitter();
    @Output() valueChange = new EventEmitter();
    private buttonState: boolean;

    constructor() {
    }

    ngOnInit() {
        this.switchOff();
    }

    ngAfterViewInit() {
        this.switchOff();
    }

    public toggleButtonState(): void {
        this.buttonState = !this.buttonState;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MyNumberToggleComponent tipToggle :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.buttonState::', this.buttonState,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( !!this.buttonState ) {
            this.switchOn();
        } else if ( !this.buttonState ) {
            this.switchOff();
        }
    }

    private switchOn(): void {
        this.buttonState = true;
        this.toggleState.emit(this.buttonState);
        this.valueChange.emit(this.buttonState);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MyNumberToggleComponent switchOn :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.buttonState::', this.buttonState,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        gsap.to(this.symbolPlus.nativeElement, 0.5, {
            autoAlpha: 0.0,
            ease: Power2.easeOut,
        });
        gsap.to(this.symbolMinus.nativeElement, 0.5, {
            autoAlpha: 1.0,
            ease: Power2.easeOut,
        });

    }

    private switchOff(): void {
        this.buttonState = false;
        this.toggleState.emit(this.buttonState);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MyNumberToggleComponent switchOff :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.buttonState::', this.buttonState,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        gsap.to(this.symbolPlus.nativeElement, 0.35, {
            autoAlpha: 1.0,
            ease: Power2.easeOut,
        });
        gsap.to(this.symbolMinus.nativeElement, 0.35, {
            autoAlpha: 0.0,
            ease: Power2.easeOut,
        });
    }
}
