import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputRangeSliderComponent } from './input-range-slider.component';

describe('InputRangeSliderComponent', () => {
  let component: InputRangeSliderComponent;
  let fixture: ComponentFixture<InputRangeSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputRangeSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputRangeSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
