import {
    Component,
    ElementRef,
    forwardRef,
    HostListener,
    Input,
    Injector,
    OnInit,
    Optional,
    Self,
    ViewChild,
} from '@angular/core';
import {
    ControlContainer,
    ControlValueAccessor,
    FormBuilder,
    FormControl,
    FormGroup,
    FormGroupDirective,
    NgControl,
    NG_VALIDATORS,
    NG_VALUE_ACCESSOR
} from '@angular/forms';
import { formatPercent, PercentPipe } from '@angular/common';
import { Draggable, InertiaPlugin, Power2, gsap } from 'gsap/all';
import { typeIsOrHasBaseType } from 'tslint/lib/language/typeUtils';

gsap.registerPlugin(Draggable);

@Component({
    selector: 'app-input-range-slider',
    templateUrl: './input-range-slider.component.html',
    styleUrls: ['./input-range-slider.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InputRangeSliderComponent),
            multi: true
        },
    ],
})

export class InputRangeSliderComponent implements ControlValueAccessor, OnInit {

    // @Input() inputRangeSliderFormControlName: string;
    @Input('value') _value = false;
    @Input() inputRangeSliderFormControl: FormControl;
    @Input() idd: string;
    @Input() valueType: string;
    @Input() validationState: null | boolean;
    @Input() minimumValue: number;
    @Input() maximumValue: number;
    @Input() stepValue: number;

    @ViewChild('inputRangeSliderWrapper', {static: true}) private inputRangeSliderWrapper: ElementRef;
    @ViewChild('rangeSlider', {static: true}) private rangeSlider: ElementRef;
    @ViewChild('inputField', {static: true}) private inputField: ElementRef;
    @ViewChild('inputFieldSymbol', {static: true}) private inputFieldSymbol: ElementRef;
    @ViewChild('invisibleText', {static: true}) invTextER: ElementRef;

    @HostListener('window:resize', ['$event'])
    public onResize(event) {
        event.target.innerWidth;
        this.resizeInput(this.value);
    }

    public percentagePiped: any;
    public control: FormControl;
    // public inputText: FormControl;
    // public percentagePiped: any;
    public formGroup: FormGroup = this.formBuilder.group({
        inputText: new FormControl(''),
        inputRange: '',
    });
    public form: FormGroup;
    public inString: string = '';
    public width: number = 64;

    get value(): any {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent get value :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._value::', this._value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return this._value;
    }

    set value(val) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent set value :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::val::', val,
        //     '\n::this._value::', this._value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this._value = val;
        // this.onTouched();
    }

    constructor(/*______________*______________*/
                // @Self() @Optional() public control: NgControl,
                private percentPipe: PercentPipe,
                private controlContainer: ControlContainer,
                private formBuilder: FormBuilder,
                private injector: Injector,
                /*______________*______________*/) {
        // this.control && (this.control.valueAccessor = this);
    }

    ngOnInit() {
        this.applyRangeStyle();
        const initialValue: number = parseFloat(this.inputRangeSliderFormControl.value);

        this.formGroup = this.formBuilder.group({
            inputText: initialValue,
            inputRange: initialValue,
        });

        const text = this.formGroup.get('inputText');
        const range = this.formGroup.get('inputRange');

        // range.valueChanges.subscribe(value => text.setValue(value, {emitEvent: false}));
        range.valueChanges.subscribe(value => text.setValue(value, {emitEvent: false}));
        text.valueChanges.subscribe(value => range.setValue(value, {emitEvent: false}));

        this.form = <FormGroup>this.controlContainer.control;
        this.value = initialValue;
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent ngOnInit() :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.form::', this.form,
            '\n::this.formGroup::', this.formGroup,
            '\n::this.inputRangeSliderFormControl::', this.inputRangeSliderFormControl,
            '\n::this.inputRangeSliderFormControl::', this.inputRangeSliderFormControl.value,
            '\n::this.value::', this.value,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        this.onValueChanged(String(initialValue));
        this.resizeInput(this.value);
    }

    private applyRangeStyle() {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent applyStyle :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.detectBrowser() === \'chrome\'::', this.detectBrowser() === 'chrome',
            '\n::detectBrowser()::', this.detectBrowser(),
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        if ( this.detectBrowser() === 'chrome' || this.detectBrowser() === 'safari' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-webkit",
            });
        } else if ( this.detectBrowser() === 'firefox' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-moz",
            });
        } else if ( this.detectBrowser() === 'edge' || this.detectBrowser() === 'ie' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-ms",
            });
        }
    }

    validate({value}: FormControl) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent validate() :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::value::', value,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        // const isNotValid = this.answer !== Number(value);
        // return isNotValid && {
        //     invalid: true
        // }
    }

    inputRangeFocus($event: any) {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent inputRangeFocus() :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::$event::', $event,
            '\n::$event.target::', $event.target,
            '\n::this.value::', this.value,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );

        if ( this.detectBrowser() === 'chrome' || this.detectBrowser() === 'safari' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-active-webkit",
            });
        } else if ( this.detectBrowser() === 'firefox' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-active-moz",
            });
        } else if ( this.detectBrowser() === 'edge' || this.detectBrowser() === 'ie' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-active-ms",
            });
        }

    }

    inputRangeLoseFocus($event: any) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent inputRangeFocus() :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$event::', $event,
        //     '\n::$event.target::', $event.target,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        if ( this.detectBrowser() === 'chrome' || this.detectBrowser() === 'safari' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-webkit",
            });
        } else if ( this.detectBrowser() === 'firefox' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-moz",
            });
        } else if ( this.detectBrowser() === 'edge' || this.detectBrowser() === 'ie' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                className: "range-slider-ms",
            });
        }

    }

    public onValueChanged(rangeSliderValue: string): void {
        const range = this.formGroup.get('inputRange');
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent changed  :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::detectBrowser()::', this.detectBrowser(),
            '\n::range::', range,
            '\n::rangeSliderValue::', rangeSliderValue,
            '\n::range.value::', range.value,
            '\n::this.valueType::', this.valueType,
            '\n::this.validationState::', this.validationState,
            '\n::this.inputRangeSliderFormControl::', this.inputRangeSliderFormControl,
            '\n::this.inputRangeSliderFormControl::', this.inputRangeSliderFormControl.validator,
            '\n::this.value::', this.value,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );

        if ( this.valueType === 'percentage' ) {
            this.percentageConversion(range.value);
        } else if ( this.valueType === 'generic' || typeof this.valueType === 'undefined' ) {
            this.genericConversion(range.value);
        }
        this.updateBarFill();
        this.validate(this.value);
    }

    public updateBarFill(): void {
        const range = this.formGroup.get('inputRange');
        let valueParsedFloated: number;
        valueParsedFloated = parseFloat(this.value);

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent updateBarFill  :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::range::', range,
            '\n::valueParsedFloated::', valueParsedFloated,
            '\n::this.maximumValue - this.minimumValue::', (this.maximumValue + this.minimumValue),
            '\n::this.valueType::', this.valueType,
            '\n::this.value::', this.value,
            '\n:: parseFloat(this.value)::', parseFloat(this.value),
            '\n::this.value / this.maximumValue::', this.value / this.maximumValue,
            '\n::((this.value - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100)::', ((this.value - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100),
            '\n::((valueParsedFloated - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100)::', ((valueParsedFloated - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100),
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );

        if ( this.detectBrowser() === 'chrome' || this.detectBrowser() === 'safari' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                background: 'linear-gradient(to right, rgba(117,208,244,1) 0%, rgb(0, 147, 209) ' + ((valueParsedFloated - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100) + '%, #fff ' + ((valueParsedFloated - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100) + '%, white 100%)',
            });
        } else if ( this.detectBrowser() === 'firefox' ) {
            gsap.set(this.rangeSlider.nativeElement, {
                background: 'linear-gradient(to right, rgba(117,208,244,1) 0%, rgb(0, 147, 209) ' + ((valueParsedFloated - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100) + '%, #fff ' + ((valueParsedFloated - this.minimumValue) / (this.maximumValue - this.minimumValue) * 100) + '%, white 100%)',
            });
        } else if ( this.detectBrowser() === 'edge' || this.detectBrowser() === 'ie' ) {
            //
        }
    }

    private percentageConversion(currentValue: any): void {
        let valueParsedFloated: number;
        let valueFraction: number;

        let valuePiped: any = 0;

        this.inputRangeSliderFormControl.patchValue(currentValue);
        // this.updateBarFill();
        valueParsedFloated = parseFloat(this.value);
        valueFraction = valueParsedFloated / 100.0;
        this.resizeInput(this.value);
        // valuePiped = this.percentPipe.transform(valueParsedFloated);
        valuePiped = this.percentPipe.transform(valueFraction);
        // this.percentagePiped = this.percentPipe.transform(this.formGroup.value.inputText);

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent percentageConversion :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.inputField.nativeElement::', this.inputField.nativeElement,
        //     '\n::this.inputField.nativeElement::', this.inputField.nativeElement.innerHTML,
        //     '\n::this.inputField.nativeElement.offsetLeft::', this.inputField.nativeElement.offsetLeft,
        //     '\n::this.inputField.nativeElement.value.length::', this.inputField.nativeElement.value.length,
        //     '\n::this.inputField.nativeElement.value.length::', this.inputField.nativeElement.value.length + "ch",
        //     '\n::this.inputFieldSymbol.nativeElement::', this.inputFieldSymbol,
        //     '\n::this.inputField.nativeElement.value::', this.inputField.nativeElement.value,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

    }

    private resizeInput(inputText) {
        this.inString = inputText;
        const inputFieldWidth: number = this.inputField.nativeElement.offsetWidth;

        setTimeout(() => {

            const minWidth: number = 4;
            const offsetAmount: number = 6;

            if ( this.invTextER.nativeElement.offsetWidth > minWidth ) {
                this.width = this.invTextER.nativeElement.offsetWidth + 2;
            } else {
                this.width = minWidth;
            }

            gsap.to(this.inputFieldSymbol.nativeElement, {
                duration: 0.15,
                // color: 'red',
                left: this.width + (inputFieldWidth / 2) + offsetAmount + 'px',
                ease: Power2.easeOut,
            });
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent resizeInput :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::inputText::', inputText,
            //     '\n::inputFieldWidth::', inputFieldWidth,
            //     '\n::this.width::', this.width,
            //     '\n::this.value::', this.value,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
        }, 0);
    }

    private genericConversion(currentValue: any): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent genericConversion :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.inputRangeSliderFormControl.patchValue(currentValue);
    }

    private detectBrowser(): string {
        const agent = window.navigator.userAgent.toLowerCase()
        switch (true) {
            case agent.indexOf('edge') > -1:
                return 'edge';
            case agent.indexOf('opr') > -1 && !!(<any>window).opr:
                return 'opera';
            case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
                return 'chrome';
            case agent.indexOf('trident') > -1:
                return 'ie';
            case agent.indexOf('firefox') > -1:
                return 'firefox';
            case agent.indexOf('safari') > -1:
                return 'safari';
            default:
                return 'other';
        }
    }

    onChange: any = () => {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent onChange :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    };
    onTouched: any = () => {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent onTouched :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    };

    registerOnChange(fn) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent registerOnChange :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::fn::', fn,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent registerOnTouched :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::fn::', fn,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.onTouched = fn;
    }

    writeValue(value) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: InputRangeSliderComponent writeValue :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::value::', value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( value ) {
            this.value = value;
        }

    }
}
