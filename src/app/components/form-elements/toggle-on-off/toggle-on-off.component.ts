import { Component, HostBinding, Input, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Power2, gsap } from 'gsap/all';

@Component({
    selector: 'app-toggle-on-off',
    templateUrl: './toggle-on-off.component.html',
    styleUrls: ['./toggle-on-off.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ToggleOnOffComponent),
            multi: true
        }
    ]
})
export class ToggleOnOffComponent implements ControlValueAccessor {

    @HostBinding('attr.id') externalId = '';

    @Input() set id(value: string) {
        this._ID = value;
        this.externalId = null;
    }

    @Input('value') _value = true;

    @ViewChild('switchWrapper', {static: true}) private switchWrapper: ElementRef;
    @ViewChild('switchContentTrue', {static: true}) private switchContentTrue: ElementRef;
    @ViewChild('switchContentFalse', {static: true}) private switchContentFalse: ElementRef;
    @ViewChild('slider', {static: true}) private slider: ElementRef;

    private _ID = '';

    get id() {
        return this._ID;
    }

    get value() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent get value :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._value::', this._value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return this._value;
    }

    set value(val) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent set value :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._value::', this._value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this._value = val;
        this.onChange(val);
        this.onTouched();
    }

    constructor() {
        // this.value = true;
        // this.writeValue(true);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    onChange: any = () => {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent onChange :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    };

    onTouched: any = () => {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent onTouched :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    };

    registerOnChange(fn) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent registerOnChange :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::fn::', fn,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent registerOnTouched :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::fn::', fn,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.onTouched = fn;
    }

    writeValue(value) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent writeValue :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::value::', value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( value ) {
            this.value = value;
        }
    }

    public switchToggle(): void {
        this.value = !this.value;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent switch :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.externalId::', this.externalId,
        //     '\n::this._ID::', this._ID,
        //     '\n::this.id::', this.id,
        //     '\n::this.value::', this.value,
        //     '\n::this._value::', this._value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        gsap.fromTo(this.slider.nativeElement,
            {
                duration: 0.0,
                autoAlpha: 0.25,
                ease: Power2.easeIn,
            },
            {
                duration: 0.26,
                autoAlpha: 1.0,
                ease: Power2.easeOut,
            });
        if ( !!this.value ) {
            this.switchOn();
        } else if ( !this.value ) {
            this.switchOff();
        }
    }

    private switchOn(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent switchOn :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        gsap.to(this.slider.nativeElement, {
            duration: 0.26,
            delay: 0.07,
            left: 0,
            ease: Power2.easeOut,
        });
        gsap.to(this.switchContentTrue.nativeElement, {
            duration: 0.15,
            delay: 0.07,
            color: 'white',
            ease: Power2.easeOut,
        });
        gsap.to(this.switchContentFalse.nativeElement, {
            duration: 0.15,
            // delay: 0.07,
            color: 'rgba(141, 141, 141, 1)',
            ease: Power2.easeOut,
        });
    }

    private switchOff(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ToggleOnOffComponent switchOff :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.value::', this.value,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        gsap.to(this.slider.nativeElement, {
            duration: 0.26,
            left: 45,
            ease: Power2.easeOut,
        });
        gsap.to(this.switchContentTrue.nativeElement, {
            duration: 0.15,
            // delay: 0.07,
            color: 'rgba(141, 141, 141, 1)',
            ease: Power2.easeOut,
        });
        gsap.to(this.switchContentFalse.nativeElement, {
            duration: 0.15,
            delay: 0.07,
            color: 'white',
            ease: Power2.easeOut,
        });
    }

}
