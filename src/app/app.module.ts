import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CurrencyPipe, PercentPipe, } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { ToggleOnOffComponent } from './components/form-elements/toggle-on-off/toggle-on-off.component';
import { ToolTipComponent } from './components/global/tool-tip/tool-tip.component';
import { InputRangeSliderComponent } from './components/form-elements/input-range-slider/input-range-slider.component';
import { ResultsComponent } from './components/results/results.component';
import { MyNumberToggleComponent } from './components/global/my-number-toggle/my-number-toggle.component';
import { ModalComponent } from './components/global/modal/modal.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';

import { TextFieldTextWidthDirective } from './directives/text-field-text-width.directive';
import { InputOnlyNumbersDirective } from './directives/input-only-numbers.directive';

import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';

@NgModule({
    declarations: [
        AppComponent,
        CalculatorComponent,
        ToggleOnOffComponent,
        ToolTipComponent,
        InputOnlyNumbersDirective,
        InputRangeSliderComponent,
        TextFieldTextWidthDirective,
        ResultsComponent,
        MyNumberToggleComponent,
        ModalComponent,
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        NoopAnimationsModule,
        MatDialogModule
    ],
    providers: [
        CurrencyPipe,
        PercentPipe,
        {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
    ],
    entryComponents: [
        ModalComponent
    ],
    bootstrap: [
        AppComponent,
    ]
})
export class AppModule {
}
