export class Utilities {
    /**
     * Utility Methods
     * A collection of commonly used methods.
     **/




    /**
     * Sets a default value for otherwise undefined variables.
     * @param arg {any} The variable to potentially set the value of.
     * @param val {any} The value to set the variable to if it is not yet defined.
     **/
    static defaultFor(arg, val): any {
        return typeof arg !== 'undefined' ? arg : val;
    };

    /**
     * Converts any JS function's built in 'arguments' array into floating point
     * numbers that can then be used in mathmatical calculations. Takes care of any
     * "number is a string" issues that could otherwise occur.
     * @param args {any} The calling function's built in array of arguments.
     **/
    static jsArgsToFloats(args): any {
        for (var i = 0; i < args.length; i++) {
            args[i] = parseFloat(args[i]);
        }
        return args;
    };

    /**
     * Convert a percentage to a decimal.
     * @param perc {any} The percentage value to convert.
     **/
    static percentToDecimal(perc): any {
        return parseFloat(perc) / 100;
    };

    /**
     * Convert a decimal to a percentage.
     * @param deci {any} The decimal value to convert.
     **/
    static decimalToPercent(deci): any {
        return 100 / parseFloat(deci);
    };

    /**
     * Rounds a number to 100's with the option of forcing  up or down.
     * Note: this function is mostly used in our set of test functions.
     * @param num              {float}  The number to round.
     * @param upOrDownOrRound  {string} An optoinal parameter expecting one of three
     *                         strings (round, up, down) to indicate the rounding
     *                         behavior you'd like. Defaults to 'round' if no value
     *                         is passed in.
     **/
    static roundToHundreds(num, upOrDownOrRound): any {
        // Set a default value if nothing was passed in.
        upOrDownOrRound = this.defaultFor(upOrDownOrRound, 'round');
        switch (upOrDownOrRound) {
            case 'up':
                num = Math.ceil(num * 100) / 100;
                break;
            case 'down':
                num = Math.floor(num * 100) / 100;
                break;
            default: // round
                num = Math.round(num * 100) / 100;
        }
        return num;
    };

}
