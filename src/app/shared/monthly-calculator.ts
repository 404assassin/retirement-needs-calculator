import * as _ from 'lodash';

interface LooseObject {
    [key: string]: any
}

export class MonthlyCalculator {

    public MonthlyCalculator() {
        let inflation = 0;
        let interest = 0;
        let retirement = 0;
        let starting = 0;
        let saved = 0;

        return {
            getAgeDiff: getAgeDiff,
            getInflation: getInflation,
            getCompoundedInflation: getCompoundedInflation,
            getSavedInflationAdjusted: getSavedInflationAdjusted,
            getMortalityData: getMortalityData,
            getPercentAlive: getPercentAlive,
            getAveragePercentAlive: getAveragePercentAlive,
            getAnnuityInterest: getAnnuityInterest,
            getPVOfDollar: getPVOfDollar,
            getPVOfPaymentWithMortality: getPVOfPaymentWithMortality,
            getPVOfYearlyPayment: getPVOfYearlyPayment,
            getPVOfMonthlyPayments: getPVOfMonthlyPayments,
            getMonthlyPayment: getMonthlyPayment,
            update: update
        };

        function update(opts) {
            inflation = opts.inflation || 0;
            interest = opts.interest || 0;
            retirement = opts.retirement || 0;
            starting = opts.starting || 0;
            saved = opts.saved || 0;
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculator update :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::opts::', opts,
            //     '\n::saved::', saved,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
        }

        function getMonthlyPayment() {
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculator getMonthlyPayment :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::saved::', saved,
            //     '\n::getPVOfMonthlyPayments()::', getPVOfMonthlyPayments(),
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
            return saved / getPVOfMonthlyPayments();
        }

        function getPVOfMonthlyPayments() {
            return (getPVOfYearlyPayment() * 12) / (1 + interest / 100.0 / 2.0);
        }

        function getPVOfYearlyPayment() {
            return _.sum(getPVOfPaymentWithMortality());
        }

        function getPVOfPaymentWithMortality() {
            let averages = getAveragePercentAlive();
            let pvs = getPVOfDollar();
            let data = _.zip(averages, pvs);

            return data;
        }

        function getPVOfDollar() {
            let mortalityData = getMortalityData();
            let years = _(mortalityData)
                .chain()
                .map('age')
                .filter(function (year) {
                    return year >= retirement;
                })
                .map(function (v, k) {
                    return 1 / Math.pow(getAnnuityInterest(), v - retirement);
                })
                .value();
            return years;
        }

        function getAveragePercentAlive() {
            let alive = _.clone(getPercentAlive());
            alive.push(0);
            let data = _(alive)
                .chain()
                .map(function (v, k) {
                    return (v + alive[k + 1]) / 2.0;
                })
                .initial() // al but last  value so it matches the size of %alive
                .value();
            return data;
        }

        function myFindWhere(array, criteria) {
            return array.find(item => Object.keys(criteria).every(key => item[key] === criteria[key]))
        }

        function getPercentAlive() {
            let mortalityData = getMortalityData();
            let retirementMortality: any = _.result(myFindWhere(mortalityData, {age: retirement}), 'mortality');
            //log('retirementMortality', retirementMortality);
            let data = _.chain(mortalityData)
                .filter(function (item) {
                    return item.age >= retirement;
                })
                .map(function (item) {
                    return item.mortality / retirementMortality;
                })
                .value();
            return data;
        }

        function getSavedInflationAdjusted() {
            return saved / getCompoundedInflation();
        }

        function getCompoundedInflation() {
            return (Math.pow(getInflation(), getAgeDiff()));
        }

        function getAnnuityInterest() {
            return 1 + interest / 100.0;
        }

        function getInflation() {
            return 1 + inflation / 100.0;
        }

        function getAgeDiff() {
            return retirement - starting;
        }

        function getMortalityData() {
            return [
                {
                    "age": 50,
                    "mortality": 10000
                },
                {
                    "age": 51,
                    "mortality": 9988.11
                },
                {
                    "age": 52,
                    "mortality": 9975.335207
                },
                {
                    "age": 53,
                    "mortality": 9961.220108
                },
                {
                    "age": 54,
                    "mortality": 9945.282156
                },
                {
                    "age": 55,
                    "mortality": 9927.221523
                },
                {
                    "age": 56,
                    "mortality": 9905.768798
                },
                {
                    "age": 57,
                    "mortality": 9879.825589
                },
                {
                    "age": 58,
                    "mortality": 9849.860078
                },
                {
                    "age": 59,
                    "mortality": 9815.631814
                },
                {
                    "age": 60,
                    "mortality": 9776.918963
                },
                {
                    "age": 61,
                    "mortality": 9732.873943
                },
                {
                    "age": 62,
                    "mortality": 9681.834752
                },
                {
                    "age": 63,
                    "mortality": 9623.521061
                },
                {
                    "age": 64,
                    "mortality": 9555.95432
                },
                {
                    "age": 65,
                    "mortality": 9480.15649
                },
                {
                    "age": 66,
                    "mortality": 9395.147927
                },
                {
                    "age": 67,
                    "mortality": 9298.593991
                },
                {
                    "age": 68,
                    "mortality": 9192.115792
                },
                {
                    "age": 69,
                    "mortality": 9076.708778
                },
                {
                    "age": 70,
                    "mortality": 8950.542526
                },
                {
                    "age": 71,
                    "mortality": 8814.565884
                },
                {
                    "age": 72,
                    "mortality": 8667.486037
                },
                {
                    "age": 73,
                    "mortality": 8506.36614
                },
                {
                    "age": 74,
                    "mortality": 8331.755962
                },
                {
                    "age": 75,
                    "mortality": 8141.025404
                },
                {
                    "age": 76,
                    "mortality": 7933.250154
                },
                {
                    "age": 77,
                    "mortality": 7708.326646
                },
                {
                    "age": 78,
                    "mortality": 7460.550194
                },
                {
                    "age": 79,
                    "mortality": 7191.492912
                },
                {
                    "age": 80,
                    "mortality": 6900.359704
                },
                {
                    "age": 81,
                    "mortality": 6586.710754
                },
                {
                    "age": 82,
                    "mortality": 6248.9574
                },
                {
                    "age": 83,
                    "mortality": 5887.511455
                },
                {
                    "age": 84,
                    "mortality": 5508.526455
                },
                {
                    "age": 85,
                    "mortality": 5109.009557
                },
                {
                    "age": 86,
                    "mortality": 4693.289449
                },
                {
                    "age": 87,
                    "mortality": 4264.862522
                },
                {
                    "age": 88,
                    "mortality": 3822.638927
                },
                {
                    "age": 89,
                    "mortality": 3377.083602
                },
                {
                    "age": 90,
                    "mortality": 2937.086756
                },
                {
                    "age": 91,
                    "mortality": 2509.464547
                },
                {
                    "age": 92,
                    "mortality": 2110.128435
                },
                {
                    "age": 93,
                    "mortality": 1740.5521
                },
                {
                    "age": 94,
                    "mortality": 1408.275483
                },
                {
                    "age": 95,
                    "mortality": 1119.873338
                },
                {
                    "age": 96,
                    "mortality": 872.3286965
                },
                {
                    "age": 97,
                    "mortality": 668.1156763
                },
                {
                    "age": 98,
                    "mortality": 501.9265786
                },
                {
                    "age": 99,
                    "mortality": 369.5424397
                },
                {
                    "age": 100,
                    "mortality": 268.0350442
                },
                {
                    "age": 101,
                    "mortality": 191.7318999
                },
                {
                    "age": 102,
                    "mortality": 133.880442
                },
                {
                    "age": 103,
                    "mortality": 91.96354668
                },
                {
                    "age": 104,
                    "mortality": 62.11751331
                },
                {
                    "age": 105,
                    "mortality": 41.27528619
                },
                {
                    "age": 106,
                    "mortality": 27.01463353
                },
                {
                    "age": 107,
                    "mortality": 17.45399264
                },
                {
                    "age": 108,
                    "mortality": 11.1467655
                },
                {
                    "age": 109,
                    "mortality": 7.036718976
                },
                {
                    "age": 110,
                    "mortality": 4.392517013
                },
                {
                    "age": 111,
                    "mortality": 2.713218226
                },
                {
                    "age": 112,
                    "mortality": 1.660155829
                },
                {
                    "age": 113,
                    "mortality": 1.007701307
                },
                {
                    "age": 114,
                    "mortality": 0.607891782
                },
                {
                    "age": 115,
                    "mortality": 0.365249346
                },
                {
                    "age": 116,
                    "mortality": 0.219149608
                },
                {
                    "age": 117,
                    "mortality": 0.131489765
                },
                {
                    "age": 118,
                    "mortality": 0.078893859
                },
                {
                    "age": 119,
                    "mortality": 0.047336315
                },
                {
                    "age": 120,
                    "mortality": 0.028401789
                }
            ];
        }
    }
}
