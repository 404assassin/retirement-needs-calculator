import { Utilities } from './utilities';

export class Calculations {
    /**
     * All retirement calculation methods.
     **/
    // var clc = {};
    /**
     * Get the numeric value of a certain percent of a number.
     * @param  {int} x
     * @param  {int} y
     * @return {int} The numeric value of the percentage.
     **/
    static getXPercentOfY(x, y) {
        return (x / 100) * y;
    };
    /**
     * Get the percentage one number is of another.
     * @param  {int} x
     * @param  {int} y
     * @return {int} The percentage.
     **/
    static xIsWhatPercentOfY(x, y) {
        return (x / y) * 100;
    };
    /**
     * Get a random integer between two numbers.
     * @param  {int} low The lowest possible number to be returned.
     * @param  {int} high The highest possible number to be returned.
     * @return {int} The random integer.
     **/
    static randomInt(low, high) {
        return Math.floor(Math.random() * (high - low) + low);
    };
    /**
     * Calculate years past 67.
     * @param  {int} age
     * @return {int} The number of years past 67.
     **/
//        not used: * @param  {int} fromAge
//        not used: * @param  {int} toAge
    static yrsPast67(age) {
        return Math.max(0, parseInt(age) - 67);
    };
    /**
     * Calculate a person's salary 1 year into the future.
     * @param  {float} wgGrw "Wage Growth" as a decimal, not a percentage.
     * @param  {float} inf "Inflation" as a decimal, not a percentage.
     * @param  {float} curSlry "Current Salary".
     * @return {float} The salary you'd likely recieve for that particular year in the future.
     **/
    static nextYrsSalary(wgGrw, inf, curSlry) {
        // let arguments = Utilities.jsArgsToFloats(arguments);
        let yrsFrmNw = 1;
        let nySlry = curSlry * Math.pow((1 + wgGrw + inf), yrsFrmNw);
        return nySlry;
    };
    /**
     * Calculate "end of years" average savings from salary.
     * @param  {float} rateOr "Rate of return" as a decimal, not a percentage.
     * @param  {float} curSlry "Current Salary".
     * @return {float} The average ammount you can presumably save based on these factors.
     **/
    static yearsSavingsFromSalary(rateOr, curSlry) {
        // let arguments = Utilities.jsArgsToFloats(arguments);
        let oneYrSvn = curSlry * rateOr;
        return oneYrSvn;
    };
    /**
     * Calculate "end of years" accumulated savings total.
     * @param  {float} inf "Inflation" as a decimal, not a percentage.
     * @param  {float} rateOr "Rate of return" as a decimal, not a percentage.
     * @param  {float} boySvng "Begining of year savings" which we calculate 1 years worth of appreciation for.
     * @param  {float} eoySvngAddition "End of year additions", additonal savings we should add to the years total.
     * @return {float} Total accumulated savings.
     **/
    static yearsAccumulatedSavings(inf, rateOr, boySvng, eoySvngAddition) {
        let totalAcSvn = boySvng + (boySvng * rateOr) + eoySvngAddition;
        return totalAcSvn;
    };
    /**
     * Calculate "End of ***YEAR 1*** social securtiy earnings" by returning the lesser
     * of the two paramaters.
     * @param curSlry        {float} Current salary.
     * @param ssContribBase  {float} Social security contribution base.
     **/
    static ssEarningsYear1(curSlry, ssContribBase) {
        return Math.min(curSlry, ssContribBase);
    };
    /**
     * Calculate "End of year social securtiy earnings"
     * @param wgGrw       {float} Wage growth as decimal, not percent.
     * @param inf         {float} Inflation as decimal, not percent.
     * @param ssEarnY1    {float} Social security earnings on year 1.
     * @param yrsAccruing {float} Number of years ss earnings have accrued since year 1.
     * @param age         {int}   Current age.
     * @param retireAge   {int}   Retirement age.
     **/
    static ssEarningsSubsequentYears(wgGrw, inf, ssEarnY1, yrsAccruing, age, retireAge) {
        let val = 0,
            isRetired = (age >= retireAge),
            is67orOlder = (age >= 67);
        if ( isRetired || is67orOlder ) {
            val = 0;
        } else {
            val = ssEarnY1 * Math.pow((1 + wgGrw + inf), yrsAccruing);
        }
        return val;
    };
    /**
     * Calculate "SS Index Earnings Ftr"
     * @param wgGrw            {float} Wage growth as decimal, not percent.
     * @param inf              {float} Inflation as decimal, not percent.
     * @param curYrsIndxErnFtr {float} Current years index earning ftr.
     * @param age              {int}   Current age.
     **/
    static ssIndexEarningFtr(wgGrw, inf, curYrsIndxErnFtr, age) {
        let val = 0,
            is67orOlder = (age >= 67),
            isBetween60to66 = (age >= 60) && (age <= 66);
        if ( is67orOlder ) {
            val = 0;
        } else if ( isBetween60to66 ) {
            val = 1;
        } else {
            val = curYrsIndxErnFtr * (1 + wgGrw + inf);
        }
        return val;
    };
    /**
     * Calculate the "social security index earnings" for the year.
     * @param {int} ssErn This years social security earnings
     * @param {int} ssIndxErnFtr This years social security indexed earning FTR.
     **/
    static ssIndexEarnings(ssErn, ssIndxErnFtr) {
        return ssErn * ssIndxErnFtr;
    };
    /**
     * Calculate "Begining of year social securtiy bend point A".
     * @param wgGrw       {float} Wage growth as decimal, not percent.
     * @param inf         {float} Inflation as decimal, not percent.
     * @param ssBptAy1    {float} Social security earnings on year 1.
     * @param yrsAccruing {float} Number of years ss bend points have been accruing.
     * Gotcha Note: because we calculate for "begining of year" think "base 0" where
     * if you want results for "year one" you pass in "0" for the param "yrsAccruing",
     * and for "year two" results, you'd pass in "1" for the param "yrsAccruing", etc.
     **/
    static yearsSsBendPointA(wgGrw, inf, ssBptAy1, yrsAccruing) {
        let val = ssBptAy1 * Math.pow((1 + wgGrw + inf), yrsAccruing);
        return val;
    };
    /**
     * Calculate "Begining of year social securtiy bend point B".
     * @param wgGrw       {float} Wage growth as decimal, not percent.
     * @param inf         {float} Inflation as decimal, not percent.
     * @param ssBptBy1    {float} Social security earnings on year 1.
     * @param yrsAccruing {float} Number of years ss bend points have been accruing.
     * Gotcha Note: because we calculate for "begining of year" think "base 0" where
     * if you want results for "year one" you pass in "0" for the param "yrsAccruing",
     * and for "year two" results, you'd pass in "1" for the param "yrsAccruing", etc.
     **/
    static yearsSsBendPointB(wgGrw, inf, ssBptBy1, yrsAccruing) {
        let val = ssBptBy1 * Math.pow((1 + wgGrw + inf), yrsAccruing);
        return val;
    };
    /**
     * Calculate "Discount percent" for the year as a decimal.
     * @param rateOr       {float} The assumed "Rate of return" as decimal, not percent.
     * @param yrsPastRtrm  {float} Years past retirement. On the year of retirement this value should be zero.
     **/
    static discountPercentAsDecimal(rateOr, yrsPastRtrm) {
        let val = Math.pow((1 + rateOr), -yrsPastRtrm);
        return val;
    };
    /**
     * Calculate "Discount value" for the year.
     * @param discPrcnt  {float} Social security "discount percent" for the year.
     * @param retmntInc  {float} Retirement income for the year.
     **/
    static discountValue(discPrcnt, retmntInc) {
        return discPrcnt * retmntInc;
    };
    /**
     * Calculate retirement income for the FIRST year of retirement.
     * @param replacementRate  {float} Replacement rate.
     * @param finalWrkYrsSlry  {float} Final work year's salary.
     **/
    static retirementIncomeYear1(replacementRate, finalWrkYrsSlry) {
        return finalWrkYrsSlry * replacementRate;
    };
    /**
     * Calculate retirement income for the year.
     * @param inf              {float} Inflation.
     * @param prevYrRtrmntInc  {float} Previous years retirement income.
     **/
    static retirementIncomeSubsequentYear(inf, prevYrRtrmntInc) {
        return prevYrRtrmntInc * (1 + inf);
    };
    /**
     * Calculate monthly or annual social security payments at 67.
     * @param inf              {float} Inflation as a decimal.
     * @param pia              {float} Primary Insurance Amount.
     * @param annualOrMonthly  {string} Optional string indicating whether you want the 'annual' or 'monthly' payment ammount returned.
     **/
    static ssPaymentAt67(inf, pia, annualOrMonthly) {
        // Figure the monthly value to start.
        let val = pia * Math.pow((1 + inf), 5);
        // Round it to the nearest dollar.
        val = Math.floor(val);
        // Make this function default to returning the annual amount.
        annualOrMonthly = Utilities.defaultFor(annualOrMonthly, 'annual');
        if ( annualOrMonthly === 'annual' ) {
            val = 12 * val;
        }
        return val;
    };
    /**
     * Calculate social security payment any year after 67.
     * @param inf  {float} Inflation as a decimal.
     * @param ssPaymentPrevYr  {float} Previous years social security payment.Ï
     * @param savingsAcum  Previous years accrued social security savings.
     **/
    static ssPaymentAfter67(inf, ssPaymentPrevYr, savingsAcum) {
        //            val * (1 + 0.023);
        return ssPaymentPrevYr * (1 + inf);
    };
    /**
     * Calculate delayed social security payment at age 67.
     * @param inf  {float} Inflation as a decimal.
     * @param ssPaymentPrevYr  {float} Previous years social security payment.
     * @param savingsAcum  Previous years accrued social security savings.
     **/
    static ssPaymentDelayedFirstYearAfter67(inf, ssPaymentPrevYr, savingsAcum) {
        let ssAmountDuringDelayedRetirement = ssPaymentPrevYr + savingsAcum;
        return ssAmountDuringDelayedRetirement;
    };
    /**
     * Calculate delayed social security payment on year after 67.
     * @param ssSavingsDelayed_a previous years delayed SS savings.
     * @param ssPaymentPrevYr  {float} Previous years social security payment.
     * @param rorPerctage  Rate of return value.
     * @param slrySvns     .
     **/
    static ssPaymentDelayedYearAfter68(ssSavingsDelayed_a, ssPaymentPrevYr, slrySvns, rorPerctage) {
//            let ssAmountDuringDelayedRetirement = (ssSavingsDelayed_a * rorPerctage) + ssPaymentPrevYr + slrySvns;
        let ssAmountDuringDelayedRetirement = ssSavingsDelayed_a * (.06 + 1) + ssPaymentPrevYr + slrySvns;
//            console.log();

        return ssAmountDuringDelayedRetirement;
    };
    /**
     * Calculate social security payment w/ discount for the year.
     * @param ssPayment                 {float} The social security .
     * @param discountPercentAsDecimal  {float} Discount percent as a decimal.
     **/
    static ssPaymentWithDiscount(ssPayment, discountPercentAsDecimal) {
        return ssPayment * discountPercentAsDecimal;
    };
    /**
     * Calculate AIME: Average Indexed Monthly Earnings
     * See http://en.wikipedia.org/wiki/Average_Indexed_Monthly_Earnings
     * See http://www.ssa.gov/oact/cola/Benefits.html#aime
     * @param indxErnsArray  {array} Array of indexed earnings for all years of employment.
     **/
    static averageIndexedMonthlyEarnings(indxErnsArray) {
        let val = 0,
            sumOfHighest35 = 0,
            i = 0,
            a = 0,
            b = 0;
        // Sort highest to lowest.
        indxErnsArray.sort(function (a, b) {
            return b - a;
        });
        // Chop off max 35 salaries.
        if ( indxErnsArray.length > 35 ) {
            indxErnsArray = indxErnsArray.slice(0, 35);
        }
        // Add them up.
        for (i = 0; i < indxErnsArray.length; i++) {
            sumOfHighest35 += indxErnsArray[i];
        }
        // Get the average of the 35 annual values as a monthly value.
        val = sumOfHighest35 / (35 * 12);
        return val;
    };
    /**
     * Calculate PIA: "Primary Insurance Amount"
     * See http://en.wikipedia.org/wiki/Primary_Insurance_Amount
     * See http://www.ssa.gov/oact/cola/piaformula.html
     * @param bpt_a  {float} Bend point A.
     * @param bpt_b  {float} Bend point B.
     * @param amie   {float} Average Indexed Monthly Earnings.
     **/
    static primaryInsuranceAmount(bpt_a, bpt_b, amie) {
        let val = 0,
            prcnt90ofBendA = 0.9 * Math.min(bpt_a, amie),
            prcnt32ofExcessUpToBptB = 0.32 * Math.max(Math.min(bpt_b - bpt_a, amie - bpt_a), 0),
            prcnt15ofExcessOverBend = 0.15 * Math.max(amie - bpt_b, 0);
        val = prcnt90ofBendA + prcnt32ofExcessUpToBptB + prcnt15ofExcessOverBend;
        val = Math.floor(val * 10) / 10; // Round to nearest 10th.
        //console.log('primaryInsuranceAmount');
        return val;
    };
    /**
     * Calculate the total amount of money you will have upon retirement.
     * @param inf                      {float} The rate of inflation as a decimal point.
     * @param yearsNowToRtrmnt         {int} The number of years between this year (today) and retirement.
     * @param svngsUponRtrmnt          {float} Total amount saved upon retirement.
     * @param todayOrTomorrowDlrs      {string} Expects 1 of two strings indicating
     *   whether to adjust for inflation to reflect todays or tommorow dollars (the
     *   forecasted value of the dollar upon the person's year of retirement).
     **/
    static willHave(inf, yearsNowToRtrmnt, svngsUponRtrmnt, todayOrTomorrowDlrs) {
        let val = svngsUponRtrmnt;
        if ( todayOrTomorrowDlrs === 'today' ) {
            val = val * Math.pow((1 + inf), -yearsNowToRtrmnt);
        }
        // Round to nearest thousand dollars.
        val = Math.round(val / 1000) * 1000;
        return val;
    };
    /**
     * Calculate the total amount of money you will need upon retirement.
     * @param inf                        {float} The rate of inflation as a decimal point.
     * @param yearsNowToRtrmnt           {int} The number of years between this year (today) and retirement.
     * @param eolSumOfDiscountedValues         {float} The end-of-life sum of all past years "discounted values".
     * @param eolSumOfSsPaymentsWithDiscount   {float} The end-of-life sum of all past years "ss payments with discount applied".
     * @param todayOrTomorrowDlrs              {string} Expects 1 of two strings indicating
     *   whether to adjust for inflation to reflect todays or tommorow dollars (the
     *   forecasted value of the dollar upon the person's year of retirement).
     **/
//         not used: * @param svngsUponRtrmnt            {float} Total amount saved upon retirement.
    static willNeed(inf, yearsNowToRtrmnt, eolSumOfDiscountedValues, eolSumOfSsPaymentsWithDiscount, todayOrTomorrowDlrs) {
        let val = eolSumOfDiscountedValues - eolSumOfSsPaymentsWithDiscount;
        if ( todayOrTomorrowDlrs === 'today' ) {
            val = val * Math.pow((1 + inf), -yearsNowToRtrmnt);
        }
        // Round to nearest thousand dollars.
        val = Math.round(val / 1000) * 1000;
        return val;
    };
    /**
     * A function to determine which type of year we are working with when looping through
     * each year in the retirement plan scenario. Allows us to pull alot of the boolean
     * logic clutter out of the getAllYears() function.
     * @param ageStart   {int} The age at which the person is/was on the first year of all years tracked.
     * @param ageRetire  {int} The age at which the person plans to retire.
     * @param ageNow     {int} The age which the person is now, during the year in question.
     * @param ageEol     {int} The assumed max 'end of life' age the person will reach.
     * @return           {obj} An object with many true/false properties indicating what type of year
     *                         this is or is not.
     **/
    static yearIs(ageStart, ageRetire, ageNow, ageEol) {
        return {
            /** all **/
            yr1: (ageNow === ageStart),
            yrAfter1: (ageNow > ageStart),
            /** 67 **/
            yrUnder67: (ageNow < 67),
            yr66: (ageNow === 66),
            yr67: (ageNow === 67),
            yr67OrOver: (ageNow >= 67),
            yrOver67: (ageNow > 67),
            /** work **/
            workYrAny: (ageNow <= ageRetire),
            workYr1: (ageNow === ageStart) && (ageStart <= ageRetire),
            workYrAfter1: (ageNow > ageStart) && (ageNow <= ageRetire),
            workYrNotLast: (ageNow < ageRetire),
            workYr1BeforeLast: (ageNow === (ageRetire - 1)),
            workYrLast: (ageNow === ageRetire),
            /** retirement **/
            retireYrAny: (ageNow >= (ageRetire + 1)),
            retireYr1: (ageNow === (ageRetire + 1)),
            retireYrAfter1: (ageNow > (ageRetire + 1)),
            retireYrNotLast: (ageNow >= (ageRetire + 1)) && (ageNow < ageEol),
            retireYrLast: (ageNow === ageEol),
            /** early & delayed **/
            pre67NonWorkYrDuringEarlyRetire: (ageRetire < 67) && (ageNow < 67) && (ageNow > ageRetire),
            post67ExtraWorkYrDuringDelayedRetire: (ageRetire > 67) && (ageNow > 67) && (ageNow < ageRetire)
        };
    };
    /**
     * Calculate the additional years of work required in early retirement neccary for the AIME array.
     * @param ssSavingsAIMEAmount      {int} The additional years required for a full 35 in early retirement.
     *   --.
     **/
//        function totalAIMEYears(ssSavingsAIMEAmount) {
//            console.log(':::::::ssSavingsAIMEAmount: ' + ssSavingsAIMEAmount);
//            let val = 0;
//            return function (x) {
//                val++;
//            };
//        }

}
