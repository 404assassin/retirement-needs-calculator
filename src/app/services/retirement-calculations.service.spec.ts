import { TestBed } from '@angular/core/testing';

import { RetirementCalculationsService } from './retirement-calculations.service';

describe('RetirementCalculationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RetirementCalculationsService = TestBed.get(RetirementCalculationsService);
    expect(service).toBeTruthy();
  });
});
