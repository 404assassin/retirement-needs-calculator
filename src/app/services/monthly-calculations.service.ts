import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class MonthlyCalculationsService {
    private inflation = 0;
    private interest = 0;
    private retirement = 0;
    private starting = 0;
    private saved = 0;

    constructor() {
    }

    public calculator() {

        return {
            getAgeDiff: this.getAgeDiff,
            getInflation: this.getInflation,
            getCompoundedInflation: this.getCompoundedInflation,
            getSavedInflationAdjusted: this.getSavedInflationAdjusted,
            getMortalityData: this.getMortalityData,
            getPercentAlive: this.getPercentAlive,
            getAveragePercentAlive: this.getAveragePercentAlive,
            getAnnuityInterest: this.getAnnuityInterest,
            getPVOfDollar: this.getPVOfDollar,
            getPVOfPaymentWithMortality: this.getPVOfPaymentWithMortality,
            getPVOfYearlyPayment: this.getPVOfYearlyPayment,
            getPVOfMonthlyPayments: this.getPVOfMonthlyPayments,
            getMonthlyPayment: this.getMonthlyPayment,
            update: this.update
        };

    }

    public update(opts) {
        this.inflation = opts.inflation || 0;
        this.interest = opts.interest || 0;
        this.retirement = opts.retirement || 0;
        this.starting = opts.starting || 0;
        this.saved = opts.saved || 0;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService update :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::opts::', opts,
        //     '\n::opts::', opts.retirement,
        //     '\n::saved::', this.saved,
        //     '\n::retirement::', this.retirement,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public getMonthlyPayment() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getMonthlyPayment :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.saved / this.getPVOfMonthlyPayments()::', this.saved / this.getPVOfMonthlyPayments(),
        //     '\n::saved::', this.saved,
        //     '\n::this.getPVOfMonthlyPayments()::', this.getPVOfMonthlyPayments(),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return this.saved / this.getPVOfMonthlyPayments();
    }

    public getPVOfMonthlyPayments() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getPVOfMonthlyPayments :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::interest::', this.interest,
        //     '\n::(this.getPVOfYearlyPayment() * 12) / (1 + this.interest / 100.0 / 2.0)::', (this.getPVOfYearlyPayment() * 12) / (1 + this.interest / 100.0 / 2.0),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return (this.getPVOfYearlyPayment() * 12) / (1 + this.interest / 100.0 / 2.0);
    }

    public getPVOfYearlyPayment() {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getPVOfYearlyPayment :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::_.sum(this.getPVOfPaymentWithMortality())::', _.sum(this.getPVOfPaymentWithMortality()),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        return _.sum(this.getPVOfPaymentWithMortality());
    }

    public getPVOfPaymentWithMortality() {
        let averages = this.getAveragePercentAlive();
        let pvs = this.getPVOfDollar();

        let data = _.zip(averages, pvs)
            .map(function (vals) {
                return vals[0] * vals[1];
            });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getPVOfPaymentWithMortality :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::averages::', averages,
        //     '\n::pvs::', pvs,
        //     '\n::_.zip(averages, pvs)::', _.zip(averages, pvs),
        //     '\n::data::', data,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return data;
    }

    public getPVOfDollar() {
        let mortalityData = this.getMortalityData();
        let annuityInterest: number = this.getAnnuityInterest();
        let retirementAge: number = this.retirement;
        let years = _(mortalityData)
            .chain()
            .map('age')
            .filter(function (year) {
                return year >= retirementAge;
            })
            .map(function (v, k) {
                return 1 / Math.pow(annuityInterest, v - retirementAge);
            })
            .value();
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getAveragePercentAlive :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::annuityInterest::', annuityInterest,
        //     '\n::years::', years,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return years;
    }

    public getAveragePercentAlive() {
        let alive = _.clone(this.getPercentAlive());
        alive.push(0);
        let data = _(alive)
            .chain()
            .map(function (v, k) {
                return (v + alive[k + 1]) / 2.0;
            })
            .initial() // al but last  value so it matches the size of %alive
            .value();
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getAveragePercentAlive :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::alive::', alive,
        //     '\n::data::', data,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return data;
    }

    public myFindWhere(array, criteria) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService myFindWhere :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::array::', array,
        //     '\n::criteria::', criteria,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return array.find(item => Object.keys(criteria).every(key => item[key] === criteria[key]))
    }

    public getPercentAlive() {
        let retirementAge: number = this.retirement;
        let mortalityData = this.getMortalityData();
        let where = mortalityData.filter(obj => {
            return obj.age === retirementAge
        });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getPercentAlive :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::mortalityData::', mortalityData,
        //     '\n::{age: retirementAge}::', {age: retirementAge},
        //     '\n::retirementAge::', retirementAge,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        // let retirement = this.retirement;
        // let where = this.myFindWhere(mortalityData, {age: this.retirement});
        // let where = this.findByMatchingProperties(mortalityData, this.retirement);
        let retirementMortality: any = _.result(this.myFindWhere(mortalityData, {age: retirementAge}), 'mortality');
        // let retirementMortality: any = _.result(where, 'mortality');
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MonthlyCalculationsService getPercentAlive :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::mortalityData::', mortalityData,
        //     '\n::where::', where,
        //     '\n::retirementAge::', retirementAge,
        //     '\n::this.retirement::', this.retirement,
        //     '\n::retirementMortality::', retirementMortality,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        //log('retirementMortality', retirementMortality);
        let data = _.chain(mortalityData)
            .filter(function (item) {
                return item.age >= retirementAge;
            })
            .map(function (item) {
                return item.mortality / retirementMortality;
            })
            .value();
        return data;
    }

    public getSavedInflationAdjusted() {
        return this.saved / this.getCompoundedInflation();
    }

    public getCompoundedInflation() {
        return (Math.pow(this.getInflation(), this.getAgeDiff()));
    }

    public getAnnuityInterest() {
        return 1 + this.interest / 100.0;
    }

    public getInflation() {
        return 1 + this.inflation / 100.0;
    }

    public getAgeDiff() {
        return this.retirement - this.starting;
    }

    public getMortalityData() {
        return [
            {
                "age": 50,
                "mortality": 10000
            },
            {
                "age": 51,
                "mortality": 9988.11
            },
            {
                "age": 52,
                "mortality": 9975.335207
            },
            {
                "age": 53,
                "mortality": 9961.220108
            },
            {
                "age": 54,
                "mortality": 9945.282156
            },
            {
                "age": 55,
                "mortality": 9927.221523
            },
            {
                "age": 56,
                "mortality": 9905.768798
            },
            {
                "age": 57,
                "mortality": 9879.825589
            },
            {
                "age": 58,
                "mortality": 9849.860078
            },
            {
                "age": 59,
                "mortality": 9815.631814
            },
            {
                "age": 60,
                "mortality": 9776.918963
            },
            {
                "age": 61,
                "mortality": 9732.873943
            },
            {
                "age": 62,
                "mortality": 9681.834752
            },
            {
                "age": 63,
                "mortality": 9623.521061
            },
            {
                "age": 64,
                "mortality": 9555.95432
            },
            {
                "age": 65,
                "mortality": 9480.15649
            },
            {
                "age": 66,
                "mortality": 9395.147927
            },
            {
                "age": 67,
                "mortality": 9298.593991
            },
            {
                "age": 68,
                "mortality": 9192.115792
            },
            {
                "age": 69,
                "mortality": 9076.708778
            },
            {
                "age": 70,
                "mortality": 8950.542526
            },
            {
                "age": 71,
                "mortality": 8814.565884
            },
            {
                "age": 72,
                "mortality": 8667.486037
            },
            {
                "age": 73,
                "mortality": 8506.36614
            },
            {
                "age": 74,
                "mortality": 8331.755962
            },
            {
                "age": 75,
                "mortality": 8141.025404
            },
            {
                "age": 76,
                "mortality": 7933.250154
            },
            {
                "age": 77,
                "mortality": 7708.326646
            },
            {
                "age": 78,
                "mortality": 7460.550194
            },
            {
                "age": 79,
                "mortality": 7191.492912
            },
            {
                "age": 80,
                "mortality": 6900.359704
            },
            {
                "age": 81,
                "mortality": 6586.710754
            },
            {
                "age": 82,
                "mortality": 6248.9574
            },
            {
                "age": 83,
                "mortality": 5887.511455
            },
            {
                "age": 84,
                "mortality": 5508.526455
            },
            {
                "age": 85,
                "mortality": 5109.009557
            },
            {
                "age": 86,
                "mortality": 4693.289449
            },
            {
                "age": 87,
                "mortality": 4264.862522
            },
            {
                "age": 88,
                "mortality": 3822.638927
            },
            {
                "age": 89,
                "mortality": 3377.083602
            },
            {
                "age": 90,
                "mortality": 2937.086756
            },
            {
                "age": 91,
                "mortality": 2509.464547
            },
            {
                "age": 92,
                "mortality": 2110.128435
            },
            {
                "age": 93,
                "mortality": 1740.5521
            },
            {
                "age": 94,
                "mortality": 1408.275483
            },
            {
                "age": 95,
                "mortality": 1119.873338
            },
            {
                "age": 96,
                "mortality": 872.3286965
            },
            {
                "age": 97,
                "mortality": 668.1156763
            },
            {
                "age": 98,
                "mortality": 501.9265786
            },
            {
                "age": 99,
                "mortality": 369.5424397
            },
            {
                "age": 100,
                "mortality": 268.0350442
            },
            {
                "age": 101,
                "mortality": 191.7318999
            },
            {
                "age": 102,
                "mortality": 133.880442
            },
            {
                "age": 103,
                "mortality": 91.96354668
            },
            {
                "age": 104,
                "mortality": 62.11751331
            },
            {
                "age": 105,
                "mortality": 41.27528619
            },
            {
                "age": 106,
                "mortality": 27.01463353
            },
            {
                "age": 107,
                "mortality": 17.45399264
            },
            {
                "age": 108,
                "mortality": 11.1467655
            },
            {
                "age": 109,
                "mortality": 7.036718976
            },
            {
                "age": 110,
                "mortality": 4.392517013
            },
            {
                "age": 111,
                "mortality": 2.713218226
            },
            {
                "age": 112,
                "mortality": 1.660155829
            },
            {
                "age": 113,
                "mortality": 1.007701307
            },
            {
                "age": 114,
                "mortality": 0.607891782
            },
            {
                "age": 115,
                "mortality": 0.365249346
            },
            {
                "age": 116,
                "mortality": 0.219149608
            },
            {
                "age": 117,
                "mortality": 0.131489765
            },
            {
                "age": 118,
                "mortality": 0.078893859
            },
            {
                "age": 119,
                "mortality": 0.047336315
            },
            {
                "age": 120,
                "mortality": 0.028401789
            }
        ];
    }

}
