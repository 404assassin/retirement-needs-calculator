import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DynamicVariablesService } from './dynamic-variables.service';
import { Utilities } from '../shared/utilities';
import { Calculations } from '../shared/calculations';
// import { MonthlyCalculator } from '../shared/monthly-calculator';
import { MonthlyCalculationsService } from './monthly-calculations.service';

interface LooseObject {
    [key: string]: any
}

@Injectable({
    providedIn: 'root'
})
export class RetirementCalculationsService {
    private messageState = new BehaviorSubject<LooseObject>({});
    public currentMessageState = this.messageState.asObservable();
    private messageValue = new BehaviorSubject<LooseObject>({});
    public currentDisplayState = this.messageValue.asObservable();

    private allYears: any;
    // private monthlyCal: MonthlyCalculator;
    private newTotal: any;
    private youMayHaveMonthly: any;
    private dynamicVariable: any;
    private ssIncomeBoolean: boolean;

    private inputs: LooseObject = {
        ageNow: 0,
        ageRetire: 0,
        salary: 0,
        savingsBase: 0,
        savingsRateAsPercent: 0,
        getCountOfWorkYears: () => {
            return (this.inputs.ageRetire - this.inputs.ageNow);
        },
        getDateOfRetire: () => {
            return (this.inputs.ageRetire - this.inputs.ageNow) + (new Date().getFullYear());
        }
    };

    /** Main outputs **/
    private output: LooseObject = {
        aime: 0,
        pia: 0,
        bptA: 0,
        bptB: 0,
        ssMnthBenAt67: 0,
        totalSvngsOnRtrmnt: 0,
        totalOfYrlDiscntValsAtEol: 0,
        // The following dollar values are in "day-of-retirement-dollars".
        willHave: 0,
        willNeed: 0,
        getSurplus: () => {
            return this.output.willHave - this.output.willNeed;
        },
        // ...while these versions are in "today-dollars".
        willHaveTdyDlr: 0,
        willNeedTdyDlr: 0,
        getSurplusTdyDlr: () => {
            return this.output.willHaveTdyDlr - this.output.willNeedTdyDlr;
        }
    };

    /** Results Display **/
    private display: LooseObject = {
        graphHaveVal: 0,
        youMayHaveMonthly: 0,
        graphNeedVal: 0,
        youMayNeedMonthly: 0,
        graphHaveValTmd: 0,
        graphNeedValTmd: 0,
        graphMaxHeight: 256,
        graphHaveStyles: {"height": "1px"},
        graphNeedStyles: {"height": "1px"},
        graphMsgStyles: {},
        graphMsg: {
            head: "",
            body: ""
        },
        naVal: '~'
    };

    private ssf: LooseObject;
    private asm: LooseObject;

    constructor(/*īīīīīīīīīī|=+=|īīīīīīīīīīī*/
                // private dynamicVariablesService: DynamicVariablesService,
                private monthlyCalculationsService: MonthlyCalculationsService,
                /*īīīīīīīīīī|=+=|īīīīīīīīīīī*/) {
        // this.getData();
    }

    public setCalculationValues(payload: LooseObject): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: RetirementCalculationsService setCalculationValues :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::payload::', payload,
        //     '\n::payload.dataAssumption::', payload.dataAssumption,
        //     // '\n::this.output::', this.output,
        //     '\n::this.messageState::', this.messageState,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.messageState.next(payload);
        if ( typeof this.messageState !== 'undefined' ) {
            this.updateInputs(payload);
            this.dataUpdate(payload);
        }
    }

    public updateInputs(payload): void {
        this.ssIncomeBoolean = payload.ssIncome;

        this.inputs = {
            ageNow: payload.currentAge,
            ageRetire: payload.retirementAge,
            salary: payload.annualEarned,
            savingsBase: payload.savedTotal,
            savingsRateAsPercent: payload.retirementPlanContribution,
            getCountOfWorkYears: () => {
                // return (this.inputs.ageRetire - this.inputs.ageNow);
                return (payload.retirementAge - payload.currentAge);
            },
            getDateOfRetire: () => {
                // return (this.inputs.ageRetire - this.inputs.ageNow) + (new Date().getFullYear());
                return (payload.retirementAge - payload.currentAge) + (new Date().getFullYear());
            }
        };
        // this.updateExternalValues();
        // this.messageValue.next(this.display);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: RetirementCalculationsService updateInputs  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::payload::', payload,
        //     '\n::payload.currentAge::', payload.currentAge,
        //     '\n::payload.retirementAge::', payload.retirementAge,
        //     '\n::payload.annualEarned::', payload.annualEarned,
        //     '\n::payload.savedTotal::', payload.savedTotal,
        //     '\n::payload.retirementPlanContribution::', payload.retirementPlanContribution,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    /** Assumptions we calculate against are imported from an external JSON filevto allow for external configuration **/
    /** Prep 'assumption' vals for use in calculations. **/
    private dataUpdate(data): void {
        let dataAssumptions: LooseObject = data.dataAssumption;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: RetirementCalculationsService dataUpdate a :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::data::', data,
        //     '\n::data.dataAssumption::', data.dataAssumption,
        //     '\n::data.dataAssumption.wage_growth::', data.dataAssumption.wage_growth,
        //     '\n::dataAssumptions.wage_growth::', dataAssumptions.wage_growth,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        this.asm = {
            wgGrw: Utilities.percentToDecimal(dataAssumptions.wage_growth),
            inf: Utilities.percentToDecimal(dataAssumptions.inflation),
            repRate: Utilities.percentToDecimal(dataAssumptions.replacement_rate),
            rateOr: Utilities.percentToDecimal(dataAssumptions.rate_of_return),
            eolAge: parseFloat(dataAssumptions.age_survive_to),
            inflationRaw: parseFloat(dataAssumptions.inflation),
            annuityRate: parseFloat(dataAssumptions.annuity_rate)
        };
        /** Prep 'social security factor' vals for use in calculations. **/

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: RetirementCalculationsService dataUpdate b (ss) :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::data.ssIncome::', data.ssIncome,
        //     '\n::Boolean(JSON.parse(dataAssumptions.ss_income_default)) === true::', Boolean(JSON.parse(dataAssumptions.ss_income_default)) === true,
        //     '\n::Boolean(JSON.parse(dataAssumptions.ss_income_default))::', Boolean(JSON.parse(dataAssumptions.ss_income_default)),
        //     '\n::data.ss_factors_for_yr::', dataAssumptions.ss_factors_for_yr,
        //     '\n::data.ss_contrib_base::', dataAssumptions.ss_contrib_base,
        //     '\n::data.ss_bpnt_a::', dataAssumptions.ss_bpnt_a,
        //     '\n::data.ss_bpnt_b::', dataAssumptions.ss_bpnt_b,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        // if ( data.ssIncome === true ) {
        this.ssf = {
            factorsYr: parseInt(dataAssumptions.ss_factors_for_yr),
            contribBase: parseFloat(dataAssumptions.ss_contrib_base),
            bptA: parseFloat(dataAssumptions.ss_bpnt_a),
            bptB: parseFloat(dataAssumptions.ss_bpnt_b)
        };
        // } else if ( data.ssIncome === false ) {
        //     this.ssf = {
        //         factorsYr: parseInt(dataAssumptions.ss_factors_for_yr),
        //         contribBase: parseFloat('0'),
        //         bptA: parseFloat(dataAssumptions.ss_bpnt_a),
        //         bptB: parseFloat(dataAssumptions.ss_bpnt_b)
        //     };
        // }
        /** Now grab the data for every year for display in table format. **/

        this.allYears = this.getAllYears();
        /** Once the data is loaded we can now initialize the display. **/
        // this.updateDisplay();
        this.updateExternalValues();
    }

    // public calculation(): BehaviorSubject<LooseObject> {
    //     console.log(
    //         '\n:::::::::::::::::::::::::::::::::::::: RetirementCalculationsService calculation :::::::::::::::::::::::::::::::::::::::::::::::::::',
    //         '\n::this::', this,
    //         '\n::this.messageState::', this.messageState,
    //         '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    //     );
    //     return this.messageState;
    // }

    private updateExternalValues(): void {
        /** Now grab the data for every year for display in table format. **/
        this.allYears = this.getAllYears();
        /** Once the data is loaded we can now initialize the display. **/
        this.updateDisplay();
    }

    /** Fired by the html "calculate" button, this function first calls
     * our main calculating function, then updates the graphs and message
     * to reflect those calculations.
     **/

    private updateDisplay(): void {
        /** Calculate or recalculate allYears data and assign the resulting
         array to the html table to reflect the "proof.xls" spreadsheet.  **/
        this.allYears = this.getAllYears();
        /** FOR DEV - Uncommented the following line to run all tests on each form submission.  **/
//            test.runAll();
        let haveGraphHeight = this.display.graphMaxHeight;
        let needGraphHeight = this.display.graphMaxHeight;
        let dollarsDifference = this.output.willHave - this.output.willNeed;
        let smallerGraphPercentSize = 0;
        /** Update the message **/
        if ( dollarsDifference === 0 ) {
            this.display.graphMsg = {
                head: "On target to reach your goal.",
                body: "You should be in good shape to reach your retirement goal if you stick to your current pace of saving.",
            };
        } else if ( dollarsDifference > 0 ) {
            this.display.graphMsg = {
                head: "Saving at a good pace.",
                body: "Making a small change to your contribution or your retirement age could help you reach your goal.",
            };
            smallerGraphPercentSize = Calculations.xIsWhatPercentOfY(this.output.willNeed, this.output.willHave);
            needGraphHeight = Math.round(Calculations.getXPercentOfY(smallerGraphPercentSize, haveGraphHeight));
        } else if ( dollarsDifference < 0 ) {
            this.display.graphMsg = {
                head: "Focus on catching up.",
                body: "Increasing your contribution or delaying retirement can make it easier to reach your goal.",
            };
            smallerGraphPercentSize = Calculations.xIsWhatPercentOfY(this.output.willHave, this.output.willNeed);
            haveGraphHeight = Math.round(Calculations.getXPercentOfY(smallerGraphPercentSize, needGraphHeight));
        }
        /** Update the graphs with the new values **/
        this.display.graphHaveStyles = {"height": haveGraphHeight + "px"};
        this.display.graphNeedStyles = {"height": needGraphHeight + "px"};
        this.display.graphMsgStyles = {};

        /** monthly **/

        // monthly
        // MonthlyCalculator.MonthlyCalculator().update({
        this.monthlyCalculationsService.update({
            inflation: this.asm.inflationRaw,
            interest: this.asm.annuityRate,
            retirement: parseInt(this.inputs.ageRetire, 10),
            starting: parseInt(this.inputs.ageNow, 10),
            saved: parseFloat(this.output.willNeedTdyDlr)
        });

        // this.newTotal = MonthlyCalculator.MonthlyCalculator().getMonthlyPayment();
        this.newTotal = this.monthlyCalculationsService.getMonthlyPayment();
        this.youMayHaveMonthly = (this.newTotal / this.output.willNeedTdyDlr) * this.output.willHaveTdyDlr;

        this.display.graphHaveVal = this.output.willHaveTdyDlr;
        this.display.youMayHaveMonthly = this.youMayHaveMonthly;
        this.display.graphNeedVal = this.output.willNeedTdyDlr;
        this.display.youMayNeedMonthly = this.newTotal;

        /*console.log(
            '\n:::::::::::::::::::::::::::::::::::::: controller $scope.updateDisplay :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::dollarsDifference::', dollarsDifference,
            '\n::output.willHaveTdyDlr::', this.output.willHaveTdyDlr,
            '\n::$scope.youMayHaveMonthly ::', this.youMayHaveMonthly,
            '\n::output.willNeedTdyDlr::', this.output.willNeedTdyDlr,
            '\n::$scope.newTotal::', this.newTotal,
            '\n::this.display::', this.display,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
        this.messageValue.next(this.display);
    };

    private getAllYears() {
        /** Two sets of iterator vars for our inner & outter loops **/
        let i = 0;
        let iMax = 0;
        let ii = 0;
        let iiMin = 0;
        /** Cache inputs **/
        let inpAge = parseInt(this.inputs.ageNow, 10);
        let inpAgeRetire = parseInt(this.inputs.ageRetire, 10);
        let inpSlry = parseInt(this.inputs.salary, 10);
        let inpSvnBase = parseInt(this.inputs.savingsBase, 10);
        let inpSvngRate = Utilities.percentToDecimal(this.inputs.savingsRateAsPercent);
        let inpWrkYrsCnt = this.inputs.getCountOfWorkYears();
        let inpRetireDate = this.inputs.getDateOfRetire();
        /** Cache assumptions **/
        let asmInf = this.asm.inf;
        // let asmWgGrw = this.asm.wgGrw;
        let asmWgGrw = Utilities.percentToDecimal(this.messageState.value.averagePayIncrease);
        let asmRepRate = this.asm.repRate;
        // let asmRateOr = this.asm.rateOr;
        let asmRateOr = Utilities.percentToDecimal(this.messageState.value.averageInvestmentGrowth);
        // let asmEolAge = this.asm.eolAge;
        let asmEolAge = this.messageState.value.lifeExpectancy;

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: RetirementCalculationsService getAllYears  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::Utilities.percentToDecimal(this.messageState.value.averageInvestmentGrowth)::', Utilities.percentToDecimal(this.messageState.value.averageInvestmentGrowth),
        //     '\n::this.asm.rateOr::', this.asm.rateOr,
        //     '\n::this.messageState.value.averageInvestmentGrowth::', this.messageState.value.averageInvestmentGrowth,
        //     '\n::this.asm.wgGrw::', this.asm.wgGrw,
        //     '\n::this.messageState.value.averagePayIncrease::', this.messageState.value.averagePayIncrease,
        //     '\n::this.asm.eolAge::', this.asm.eolAge,
        //     '\n::this.messageState.value.lifeExpectancy::', this.messageState.value.lifeExpectancy,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        /** Cach ss factors **/
        let ssContribBase = this.ssf.contribBase;
        let ssBptA = this.ssf.bptA;
        let ssBptB = this.ssf.bptB;
        // Cach display settings **/
        let strNa = this.display.naVal;
        /** Vars to store "year data" as we loop through each year and collect "all year data" into the "allYrs" array. **/
        let allYrs = [];
        let lastYr: any = false;
        /** NOTE: The following "dedictedIndexedEarningsArray" is a variable to store
         'indexed earnings' that we will calculate on a inner loop that fires at
         the age/year of 67 and reverse/loops back through the years to gather
         then calculate the required info. **/
        let dedictedIndexedEarningsArray = [];
        /** A few variables to store final "umbrella" calculations which require
         that we look back at all collected year data in order to detrmine
         their value. **/
        let finalTotalOfYrlyDiscntValsAtEol = 0;
        let finalTotalOfSsPaymentsWithDiscount = 0;
        let finalSvngsUponRtrmntInTmrwDlrs: any = 0;
        /** We initialize our curYear[FOO] vals here so they reflect the first year.
         After loop one, for all subsequent years, these variables are calculated
         a bit differently. **/
        let curYrIs = Calculations.yearIs(inpAge, inpAgeRetire, inpAge, asmEolAge);
        let curYrAge = inpAge;
        /** Initialize pre retire values **/
        let curYrSlry = curYrIs.retireYrAfter1 ? strNa : inpSlry;
        let curYrSlrySvns = curYrIs.retireYrAny ? strNa : Calculations.yearsSavingsFromSalary(inpSvngRate, curYrSlry);
        let curYrSvnsAcumEoy = curYrIs.retireYrAny ? strNa : Calculations.yearsAccumulatedSavings(asmInf, asmRateOr, inpSvnBase, 0) + curYrSlrySvns;
        let curYrSsEarn;
        // if ( !!this.ssIncomeBoolean ) {
        curYrSsEarn = curYrIs.yr67OrOver ? strNa : Calculations.ssEarningsYear1(curYrSlry, ssContribBase);
        // } else {
        //
        //     curYrSsEarn = 0;
        // }
        let curYrSsBpA = curYrIs.yr67OrOver ? strNa : Calculations.yearsSsBendPointA(asmWgGrw, asmInf, ssBptA, i);
        let curYrSsBpB = curYrIs.yr67OrOver ? strNa : Calculations.yearsSsBendPointB(asmWgGrw, asmInf, ssBptB, i);
        /** Initialize post retire values **/
        let curYrRtrmntInc = strNa;
        let isCollapsed = true; // ...for now assuming year1 will never be a retirement year.
        let curYrDiscPcnt = curYrIs.workYrNotLast ? strNa : Calculations.discountPercentAsDecimal(asmRateOr, curYrAge - inpAgeRetire);
        let curYrDiscVal = strNa; // ...for now assuming year1 will never be a retirement year.
        let curYrYr = new Date().getFullYear();
        let curYrYrsAccrued = 0;
        let curYrYrsPast67 = inpAge < 67 ? strNa : Calculations.yrsPast67(inpAge);
        let curYrSsPymnt = strNa;
        let curYrSsPymntWDisc = strNa;
        let curYrSsIdxErnFtr = strNa;
        let curYrSsIdxErn = strNa;
        /** Stash this value for out subsequent years/loops to use in calculations **/
        let firstYrSsEarn = curYrSsEarn;
        /** Begin looping through the years.
         For each year between now and EOL. **/
        let ssSavingsDelayed_1: any = 'undefined';
        let ssSavingsDelayed_2: any = 'undefined';
        let extraYears = [0];
        for (i = 0, iMax = asmEolAge - inpAge; i <= iMax; i++) {
            /** If this is not year 1 (for which we instantiate the above variables for already)....**/
            if ( lastYr !== false ) {
                /** ...then update all our variables to reflect this subsequent year. **/
                curYrIs = Calculations.yearIs(inpAge, inpAgeRetire, inpAge + i, asmEolAge);
                curYrAge = lastYr.age + 1;
                /** Update pre retire values **/
                if ( curYrIs.workYrNotLast ) {
                    curYrSlry = Calculations.nextYrsSalary(asmWgGrw, asmInf, lastYr.slry);
                } else if ( curYrIs.workYrLast ) {
                    curYrSlry = Calculations.nextYrsSalary(0, asmInf, lastYr.slry); // asmWgGrw is 0 for last working year.
                } else {
                    curYrSlry = strNa;
                }
                curYrSlrySvns = curYrIs.retireYrAny ? strNa : Calculations.yearsSavingsFromSalary(inpSvngRate, curYrSlry);
                curYrSvnsAcumEoy = curYrIs.retireYrAny ? strNa : Calculations.yearsAccumulatedSavings(asmInf, asmRateOr, lastYr.svns_acum_eoy, curYrSlrySvns);
                curYrSsEarn = curYrIs.yr67OrOver ? strNa : Calculations.ssEarningsSubsequentYears(asmWgGrw, asmInf, firstYrSsEarn, i, curYrAge, inpAgeRetire);
                curYrSsBpA = curYrIs.yr67OrOver ? strNa : Calculations.yearsSsBendPointA(asmWgGrw, asmInf, ssBptA, i);
                curYrSsBpB = curYrIs.yr67OrOver ? strNa : Calculations.yearsSsBendPointB(asmWgGrw, asmInf, ssBptB, i);
                /** Update the post retire values **/
                if ( curYrIs.workYrLast ) {
                    curYrRtrmntInc = Calculations.retirementIncomeYear1(asmRepRate, curYrSlry);
                } else if ( curYrIs.retireYrAny ) {
                    curYrRtrmntInc = Calculations.retirementIncomeSubsequentYear(asmInf, lastYr.rtm_inc);
                } else {
                    curYrRtrmntInc = strNa;
                }
                curYrDiscPcnt = curYrIs.workYrNotLast ? strNa : Calculations.discountPercentAsDecimal(asmRateOr, curYrAge - inpAgeRetire);
                curYrDiscVal = curYrIs.workYrNotLast ? strNa : Calculations.discountValue(curYrDiscPcnt, curYrRtrmntInc);
                curYrYr = lastYr.year + 1;
                curYrYrsAccrued = lastYr.yrs_accrued + 1;
                curYrYrsPast67 = curYrIs.yrUnder67 ? strNa : Calculations.yrsPast67(lastYr.age + 1);
            } // end if

            /** ----------------------------------------
             If this is a STANDARD retirement year
             ----------------------------------------- **/
            if ( curYrIs.yr67 ) {
//                    /** At age 67 we can now calculate "Indexed Earing" data by
//                     looping back through all work year data previous to age 66. **/
                for (ii = allYrs.length - 1, iiMin = 0; ii >= iiMin; ii--) {
//                        /** At age 62 record "bend points" for this standard "age of eligibility".**/
                    if ( allYrs[ii].age === 62 ) {
                        this.output.bptA = Math.round(allYrs[ii].ss_bp_a);
                        this.output.bptB = Math.round(allYrs[ii].ss_bp_b);
                    }
//                        /** At age 66 calculate our initial "IndexEarningsFTR" as "1" **/
                    if ( allYrs[ii].age === 66 ) {
                        allYrs[ii].ss_idx_ern_ftr = 1;
                    } else {
                        /** The for all prior years we loop back through calculate "IndexEarningsFTR" for them
                         based on the CHRONOLOGICALLY-NEXT years (PREVIOUS in our loop) value for IndexEarningsFTR. **/
                        allYrs[ii].ss_idx_ern_ftr = Calculations.ssIndexEarningFtr(asmWgGrw, asmInf, allYrs[ii + 1].ss_idx_ern_ftr, allYrs[ii].age);
                    }
//                        /** Now calculate the actual IndexEarnings from the IndexEarningsFTR and add it to our dedicated array **/
                    allYrs[ii].ss_idx_ern = Calculations.ssIndexEarnings(allYrs[ii].ss_earn, allYrs[ii].ss_idx_ern_ftr);
                    if ( isNaN(allYrs[ii].ss_idx_ern) || allYrs[ii].ss_idx_ern === 0 ) {
                        extraYears.push(0);
                    } else {
                        dedictedIndexedEarningsArray.push(allYrs[ii].ss_idx_ern);
                    }
                }
                for (var j = 0, k = extraYears.length; j < k; j++) {
                    dedictedIndexedEarningsArray.push(allYrs[j].ss_idx_ern);
                }
//                    /** Now that all IndexedEarning data is gathered, we can calculate AIME & PIA,
//                     two of our main/final retirement factors needed to determine other key final values. **/
                this.output.aime = Calculations.averageIndexedMonthlyEarnings(dedictedIndexedEarningsArray);
                this.output.pia = Calculations.primaryInsuranceAmount(this.output.bptA, this.output.bptB, this.output.aime);
                // Note: We capture both 'annual' & 'monthly' benifits for display & calculation purposes.
                this.output.ssMnthBenAt67 = Calculations.ssPaymentAt67(asmInf, this.output.pia, 'monthly');
                curYrSsPymnt = Calculations.ssPaymentAt67(asmInf, this.output.pia, 'annual');
                curYrSsPymntWDisc = Calculations.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
                ssSavingsDelayed_1 = curYrSsPymnt + curYrSvnsAcumEoy;
                ssSavingsDelayed_2 = curYrSsPymnt;
            }

            /** 1 Year Before Retirement Year **/
            if ( curYrIs.workYr1BeforeLast ) {
                finalSvngsUponRtrmntInTmrwDlrs = curYrSvnsAcumEoy;
            }

            /** --------------------------------------
             If this is a DELAYED retirement year
             ----------------------------------------- **/
            if ( curYrIs.post67ExtraWorkYrDuringDelayedRetire ) {
//                    finalSvngsUponRtrmntInTmrwDlrs = (lastYr.svns_acum_eoy + ssSavingsDelayed_2);
                if ( curYrAge <= inpAgeRetire ) {
//                       :: the below needs to be cyphered before the the rest of this conditional ::
                    ssSavingsDelayed_1 = (ssSavingsDelayed_1 * 1.06) + (curYrSlrySvns + (curYrSsPymnt * 1.023));
                    ssSavingsDelayed_2 = (ssSavingsDelayed_2 * 1.06) + (curYrSsPymnt * 1.023);
//                       :: the above needs to be cyphered before the the rest of this conditional ::
                }
                curYrSsPymnt = curYrSsPymnt * 1.023;
                curYrSsPymntWDisc = Calculations.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
            }
            /** --------------------------------------
             This is the first retirement year
             ----------------------------------------- **/
            if ( curYrAge === inpAgeRetire ) {
                /** At age 67 we can now calculate "Indexed Earing" data by
                 looping back through all work year data previous to age 66. **/
                let iii = allYrs.length - 1, iiiMin = 0;
                for (; iii >= iiMin; iii--) {
                    /** At age 62 record "bend points" for this standard "age of eligibility".**/
                    if ( allYrs[iii].age === 62 ) {
                        this.output.bptA = Math.round(allYrs[iii].ss_bp_a);
                        this.output.bptB = Math.round(allYrs[iii].ss_bp_b);
                    }
                    /** At retirement age calculate our initial "IndexEarningsFTR" as "1" **/
                    if ( allYrs[iii].age === inpAgeRetire - 1 ) {
                        allYrs[iii].ss_idx_ern_ftr = 1;
                    } else {
                        /** The for all prior years we loop back through calculate "IndexEarningsFTR" for them
                         based on the CHRONOLOGICALLY-NEXT years (PREVIOUS in our loop) value for IndexEarningsFTR. **/
                        allYrs[iii].ss_idx_ern_ftr = Calculations.ssIndexEarningFtr(asmWgGrw, asmInf, allYrs[iii + 1].ss_idx_ern_ftr, allYrs[iii].age);
                    }
                }
            }
            /** --------------------------------------
             Calculate SS payments for any year following the age of retirement
             ----------------------------------------- **/

            if ( !!this.ssIncomeBoolean ) {
                if ( (curYrAge + 1) > inpAgeRetire && curYrAge !== 67 ) {
                    curYrSsPymnt = curYrSsPymnt * 1.023;
                    curYrSsPymntWDisc = Calculations.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
                }
            } else {
                if ( (curYrAge + 1) > inpAgeRetire && curYrAge !== 67 ) {
                    curYrSsPymnt = 0;
                    curYrSsPymntWDisc = Calculations.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
                }
            }
            /** --------------------------------------
             If this is the first DELAYED retirement year where we adjust the 'you will have' total to include additional late retirement savings.
             ----------------------------------------- **/
            if ( curYrAge === inpAgeRetire && curYrAge > 67 ) {
                finalSvngsUponRtrmntInTmrwDlrs = lastYr.svns_acum_eoy + ssSavingsDelayed_2;
            }
            /** --------------------------------------
             This year's is now collected. Add it to our array.
             ----------------------------------------- **/
            allYrs[i] = {
                year: curYrYr,
                yrs_accrued: curYrYrsAccrued,
                yrs_past67: curYrYrsPast67,
                age: curYrAge,
                rtm_inc: curYrRtrmntInc,
                slry: curYrSlry,
                slry_svns: curYrSlrySvns,
                svns_acum_eoy: curYrSvnsAcumEoy,
                disc_pcnt: curYrDiscPcnt,
                disc_val: curYrDiscVal,
                ss_earn: curYrSsEarn,
                ss_bp_a: curYrSsBpA,
                ss_bp_b: curYrSsBpB,
                ss_pymnt: curYrSsPymnt,
                ss_pymnt_wdisc: curYrSsPymntWDisc,
                ss_idx_ern_ftr: curYrSsIdxErnFtr,
                ss_idx_ern: curYrSsIdxErn,
                delayedRetire_1: ssSavingsDelayed_1,
                delayedRetire_2: ssSavingsDelayed_2
            };
            /** Prep for the next loop. This-year now becomes last-year. **/
            lastYr = allYrs[i];
        } // End outermost loop

        /** Now that we have all year data stored in allYrs[]
         we can calculate our final numbers.**/

        /** Reuse "i" as we are finished using it in the above loop. **/
        for (i = 0, iMax = allYrs.length - 1; i <= iMax; i++) {
            /** Get the totals needed to calculate other values  **/
            finalTotalOfYrlyDiscntValsAtEol += isNaN(allYrs[i].disc_val) ? 0 : allYrs[i].disc_val;
            finalTotalOfSsPaymentsWithDiscount += isNaN(allYrs[i].ss_pymnt_wdisc) ? 0 : allYrs[i].ss_pymnt_wdisc;
        }
        /** Display our key totals **/
        this.output.totalSvngsOnRtrmnt = Calculations.willHave(asmInf, inpWrkYrsCnt, finalSvngsUponRtrmntInTmrwDlrs, 'tomorrow');
        this.output.totalOfYrlDiscntValsAtEol = finalTotalOfYrlyDiscntValsAtEol;
        /** Calculate the amount they will have upon retirement. **/
        this.output.willHave = Calculations.willHave(asmInf, inpWrkYrsCnt, this.output.totalSvngsOnRtrmnt, 'tomorrow');
        this.output.willHaveTdyDlr = Calculations.willHave(asmInf, inpWrkYrsCnt, this.output.totalSvngsOnRtrmnt, 'today');
        /** Calculate the ammount they will need upon retirement. **/
        this.output.willNeed = Calculations.willNeed(asmInf, inpWrkYrsCnt, finalTotalOfYrlyDiscntValsAtEol, finalTotalOfSsPaymentsWithDiscount, 'tomorrow');
        this.output.willNeedTdyDlr = Calculations.willNeed(asmInf, inpWrkYrsCnt, finalTotalOfYrlyDiscntValsAtEol, finalTotalOfSsPaymentsWithDiscount, 'today');
        /** All year gathering and subsequent computations are now done.
         Return our years array for display in the table **/
        return allYrs;
    };

    // end getAllYears();

}

/*///////////////////////////////////////////////////////====///////////////////////////////////////////////////////*/
/*///////////////////////////////////////////////////////====///////////////////////////////////////////////////////*/
/*///////////////////////////////////////////////////////====///////////////////////////////////////////////////////*/
/*///////////////////////////////////////////////////////====///////////////////////////////////////////////////////*/

/*

/!** Initialize our controller along with dependencies **!/
var calculateControllers = angular.module('calculateControllers', ['ngAnimate']);
/!** Define our controller **!/
calculateControllers.controller('CalculatorController', ['$scope', '$http', 'monthlyCal', function ($scope, $http, monthlyCal) {
    /!** Visitor Inputs **!/
    $scope.inputs = {
        ageNow: 30,
        ageRetire: 67,
        salary: 25000,
        savingsBase: 5000,
        savingsRateAsPercent: 1,
        getCountOfWorkYears: function () {
            return (this.ageRetire - this.ageNow);
        },
        getDateOfRetire: function () {
            return (this.ageRetire - this.ageNow) + (new Date().getFullYear());
        }
    };
    /!**
     * Slider Options
     * Set post and pre dimesions accordingly
     * **!/
    $scope.options = {
        ageNow: {from: 20, to: 70, step: 1, preDimension: '', postDimension: ' years'},
        ageRetire: {from: 50, to: 70, step: 1, preDimension: '', postDimension: ' years'},
        salary: {from: 20000, to: 300000, step: 1000, preDimension: '$ ', calculate: function (val) {
                while (/(\d+)(\d{3})/.test(val.toString())) {
                    val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                }
                return val;
            }, postDimension: ''},
        savingsBase: {from: 0, to: 800000, step: 1000, preDimension: '$ ', calculate: function (val) {
                while (/(\d+)(\d{3})/.test(val.toString())) {
                    val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                }
                return val;
            }, postDimension: ''},
        savingsRate: {from: 1, to: 35, step: 1, preDimension: '', postDimension: ' %'}
    };
    /!** Main outputs **!/
    $scope.output = {
        aime: 0,
        pia: 0,
        bptA: 0,
        bptB: 0,
        ssMnthBenAt67: 0,
        totalSvngsOnRtrmnt: 0,
        totalOfYrlDiscntValsAtEol: 0,
        // The following dollar values are in "day-of-retirement-dollars".
        willHave: 0,
        willNeed: 0,
        getSurplus: function () {
            return this.willHave - this.willNeed;
        },
        // ...while these versions are in "today-dollars".
        willHaveTdyDlr: 0,
        willNeedTdyDlr: 0,
        getSurplusTdyDlr: function () {
            return this.willHaveTdyDlr - this.willNeedTdyDlr;
        }
    };
    /!** Results Display **!/
    $scope.display = {
        graphHaveVal: 123456,
        graphNeedVal: 12345,
        graphHaveValTmd: 123456,
        graphNeedValTmd: 12345,
        graphMaxHeight: 340,
        graphHaveStyles: {"height": "265px"},
        graphNeedStyles: {"height": "265px"},
        graphMsgStyles: {},
        graphMsg: "Retirement Plan",
        naVal: '~'
    };
    /!** Assumptions we calculate against are imported from an external JSON file
     to allow for external configuration **!/
    $http.get('js/data-assumptions.json').success(function (data) {
        /!** Prep 'assumption' vals for use in calculations. **!/
        $scope.asm = {
            wgGrw: utl.percentToDecimal(data.wage_growth),
            inf: utl.percentToDecimal(data.inflation),
            repRate: utl.percentToDecimal(data.replacement_rate),
            rateOr: utl.percentToDecimal(data.rate_of_return),
            eolAge: parseFloat(data.age_survive_to),
            inflationRaw: parseFloat(data.inflation),
            annuityRate: parseFloat(data.annuity_rate)
        };
        /!** Prep 'social security factor' vals for use in calculations. **!/
        $scope.ssf = {
            factorsYr: parseInt(data.ss_factors_for_yr),
            contribBase: parseFloat(data.ss_contrib_base),
            bptA: parseFloat(data.ss_bpnt_a),
            bptB: parseFloat(data.ss_bpnt_b)
        };
        /!** Now grab the data for every year for display in table format. **!/
        $scope.allYears = $scope.getAllYears();
        /!** Once the data is loaded we can now initialize the display. **!/
        $scope.updateDisplay();
    });
    /!** Fired by the html "calculate" button, this function first calls
     * our main calculating function, then updates the graphs and message
     * to reflect those calculations.
     **!/


    $scope.updateDisplay = function () {
        /!** Calculate or recalculate allYears data and assign the resulting
         array to the html table to reflect the "proof.xls" spreadsheet.  **!/
        $scope.allYears = $scope.getAllYears();
        /!** FOR DEV - Uncommented the following line to run all tests on each form submission.  **!/
//            test.runAll();
        var haveGraphHeight = $scope.display.graphMaxHeight,
            needGraphHeight = $scope.display.graphMaxHeight,
            dollarsDifference = $scope.output.willHave - $scope.output.willNeed,
            smallerGraphPercentSize = 0;
        /!** Update the message **!/
        if (dollarsDifference === 0) {
            $scope.display.graphMsg = "Spot on!";
            /!** The graphs will be left the same. **!/
        } else if (dollarsDifference > 0) {
            $scope.display.graphMsg = "Looking good!";
            /!** The need graph should be smaller... **!/
            smallerGraphPercentSize = clc.xIsWhatPercentOfY($scope.output.willNeed, $scope.output.willHave);
            needGraphHeight = Math.round(clc.getXPercentOfY(smallerGraphPercentSize, haveGraphHeight));
        } else if (dollarsDifference < 0) {
            $scope.display.graphMsg = "You've got some saving to do.";
            // The have graph should be smaller... **!/
            smallerGraphPercentSize = clc.xIsWhatPercentOfY($scope.output.willHave, $scope.output.willNeed);
            haveGraphHeight = Math.round(clc.getXPercentOfY(smallerGraphPercentSize, needGraphHeight));
        }
        /!** Update the graphs with the new values **!/
        $scope.display.graphHaveStyles = {"height": haveGraphHeight + "px"};
        $scope.display.graphNeedStyles = {"height": needGraphHeight + "px"};
        $scope.display.graphMsgStyles = {};

        // monthly
        monthlyCal.update({
            inflation: $scope.asm.inflationRaw,
            interest: $scope.asm.annuityRate,
            retirement: parseInt($scope.inputs.ageRetire, 10),
            starting: parseInt($scope.inputs.ageNow, 10),
            saved: parseFloat($scope.output.willNeedTdyDlr)
        });
        $scope.newTotal = monthlyCal.getMonthlyPayment();
        $scope.youMayHaveMonthly = ($scope.newTotal / $scope.output.willNeedTdyDlr) * $scope.output.willHaveTdyDlr;
    };
    /!**
     * Fired by the html "calculate" button, this function first calls
     * our main calculating function, then updates the graphs and message
     * to reflect those calculations.
     **!/
    var isCollapsed = false;
    var eleHeight;
    var bodyWidth;
    $scope.$watch(function () {
        return window.innerWidth;
    }, function (value) {
        //console.log(value);
        bodyWidth = value;
    });


    $scope.selectLink = function (MyTarget) {
        var ele = document.getElementById(MyTarget);
        var icon = document.getElementById('caretSVG');

        //console.log('bodyWidth: ' + bodyWidth);
        if (bodyWidth < 450) {
            eleHeight = '2150px';
        } else if (bodyWidth < 650) {
            eleHeight = '1650px';
        } else if (bodyWidth < 750) {
            eleHeight = '1050px';
        } else if (bodyWidth < 1250) {
            eleHeight = '950px';
        } else {
            eleHeight = '850px';
        }
        //console.log('eleHeight: ' + eleHeight);
        if (isCollapsed) {
            TweenMax.to(icon, .75, {
                css: {
                    transformOrigin: "50% 50%",
                    rotation: 0
                },
                ease: Cubic.easeOut
            });
            TweenMax.to(ele, .75, {
                css: {
                    autoAlpha: 0,
                    height: "10px"
                },
                ease: Cubic.easeOut
            });
            isCollapsed = !isCollapsed;
        } else if (!isCollapsed) {
            TweenMax.to(icon, .75, {
                css: {
                    transformOrigin: "50% 50%",
                    rotation: 90
                },
                ease: Cubic.easeOut
            });
            TweenMax.to(ele, 1.25, {
                css: {
                    autoAlpha: 1,
                    height: eleHeight
                },
                ease: Cubic.easeOut
            });
            isCollapsed = !isCollapsed;
        }
    };
    /!***
     * Store all utlity functions in the "utl" object.
     **!/
    var utl = {};
    /!***
     * Sets a default value for otherwise undefined variables.
     * @param arg {any} The variable to potentially set the value of.
     * @param val {any} The value to set the variable to if it is not yet defined.
     **!/
    utl.defaultFor = function (arg, val) {
        return typeof arg !== 'undefined' ? arg : val;
    };
    /!***
     * Converts any JS function's built in 'arguments' array into floating point
     * numbers that can then be used in mathmatical calculations. Takes care of any
     * "number is a string" issues that could otherwise occur.
     * @param args {any} The calling function's built in array of arguments.
     **!/
    utl.jsArgsToFloats = function (args) {
        for (var i = 0; i < args.length; i++) {
            args[i] = parseFloat(args[i]);
        }
        return args;
    };
    /!***
     * Convert a percentage to a decimal.
     * @param perc {any} The percentage value to convert.
     **!/
    utl.percentToDecimal = function (perc) {
        return parseFloat(perc) / 100;
    };
    /!***
     * Convert a decimal to a percentage.
     * @param deci {any} The decimal value to convert.
     **!/
    utl.decimalToPercent = function (deci) {
        return 100 / parseFloat(deci);
    };
    /!***
     * Rounds a number to 100's with the option of forcing  up or down.
     * Note: this function is mostly used in our set of test functions.
     * @param num              {float}  The number to round.
     * @param upOrDownOrRound  {string} An optoinal parameter expecting one of three
     *                         strings (round, up, down) to indicate the rounding
     *                         behavior you'd like. Defaults to 'round' if no value
     *                         is passed in.
     **!/
    utl.roundToHundreds = function (num, upOrDownOrRound) {
        // Set a default value if nothing was passed in.
        upOrDownOrRound = utl.defaultFor(upOrDownOrRound, 'round');
        switch (upOrDownOrRound) {
            case 'up':
                num = Math.ceil(num * 100) / 100;
                break;
            case 'down':
                num = Math.floor(num * 100) / 100;
                break;
            default: // round
                num = Math.round(num * 100) / 100;
        }
        return num;
    };



///////////////////////////////////////////////////////====///////////////////////////////////////////////////////


    /!***
     * Store all calculation functions in the "clc" object.
     **!/
    var clc = {};
    /!***
     * Get the numeric value of a certain percent of a number.
     * @param  {int} x
     * @param  {int} y
     * @return {int} The numeric value of the percentage.
     **!/
    clc.getXPercentOfY = function (x, y) {
        return (x / 100) * y;
    };
    /!***
     * Get the percentage one number is of another.
     * @param  {int} x
     * @param  {int} y
     * @return {int} The percentage.
     **!/
    clc.xIsWhatPercentOfY = function (x, y) {
        return (x / y) * 100;
    };
    /!***
     * Get a random integer between two numbers.
     * @param  {int} low The lowest possible number to be returned.
     * @param  {int} high The highest possible number to be returned.
     * @return {int} The random integer.
     **!/
    clc.randomInt = function (low, high) {
        return Math.floor(Math.random() * (high - low) + low);
    };
    /!***
     * Calculate years past 67.
     * @param  {int} age
     * @return {int} The number of years past 67.
     **!/
//        not used: * @param  {int} fromAge
//        not used: * @param  {int} toAge
    clc.yrsPast67 = function (age) {
        return  Math.max(0, parseInt(age) - 67);
    };
    /!***
     * Calculate a person's salary 1 year into the future.
     * @param  {float} wgGrw "Wage Growth" as a decimal, not a percentage.
     * @param  {float} inf "Inflation" as a decimal, not a percentage.
     * @param  {float} curSlry "Current Salary".
     * @return {float} The salary you'd likely recieve for that particular year in the future.
     **!/
    clc.nextYrsSalary = function (wgGrw, inf, curSlry) {
        var arguments = utl.jsArgsToFloats(arguments),
            yrsFrmNw = 1,
            nySlry = curSlry * Math.pow((1 + wgGrw + inf), yrsFrmNw);
        return nySlry;
    };
    /!***
     * Calculate "end of years" average savings from salary.
     * @param  {float} rateOr "Rate of return" as a decimal, not a percentage.
     * @param  {float} curSlry "Current Salary".
     * @return {float} The average ammount you can presumably save based on these factors.
     **!/
    clc.yearsSavingsFromSalary = function (rateOr, curSlry) {
        var arguments = utl.jsArgsToFloats(arguments),
            oneYrSvn = curSlry * rateOr;
        return oneYrSvn;
    };
    /!***
     * Calculate "end of years" accumulated savings total.
     * @param  {float} inf "Inflation" as a decimal, not a percentage.
     * @param  {float} rateOr "Rate of return" as a decimal, not a percentage.
     * @param  {float} boySvng "Begining of year savings" which we calculate 1 years worth of appreciation for.
     * @param  {float} eoySvngAddition "End of year additions", additonal savings we should add to the years total.
     * @return {float} Total accumulated savings.
     **!/
    clc.yearsAccumulatedSavings = function (inf, rateOr, boySvng, eoySvngAddition) {
        var totalAcSvn = boySvng + (boySvng * rateOr) + eoySvngAddition;
        return totalAcSvn;
    };
    /!***
     * Calculate "End of ***YEAR 1*** social securtiy earnings" by returning the lesser
     * of the two paramaters.
     * @param curSlry        {float} Current salary.
     * @param ssContribBase  {float} Social security contribution base.
     **!/
    clc.ssEarningsYear1 = function (curSlry, ssContribBase) {
        return Math.min(curSlry, ssContribBase);
    };
    /!***
     * Calculate "End of year social securtiy earnings"
     * @param wgGrw       {float} Wage growth as decimal, not percent.
     * @param inf         {float} Inflation as decimal, not percent.
     * @param ssEarnY1    {float} Social security earnings on year 1.
     * @param yrsAccruing {float} Number of years ss earnings have accrued since year 1.
     * @param age         {int}   Current age.
     * @param retireAge   {int}   Retirement age.
     **!/
    clc.ssEarningsSubsequentYears = function (wgGrw, inf, ssEarnY1, yrsAccruing, age, retireAge) {
        var val = 0,
            isRetired = (age >= retireAge),
            is67orOlder = (age >= 67);
        if (isRetired || is67orOlder) {
            val = 0;
        } else {
            val = ssEarnY1 * Math.pow((1 + wgGrw + inf), yrsAccruing);
        }
        return val;
    };
    /!***
     * Calculate "SS Index Earnings Ftr"
     * @param wgGrw            {float} Wage growth as decimal, not percent.
     * @param inf              {float} Inflation as decimal, not percent.
     * @param curYrsIndxErnFtr {float} Current years index earning ftr.
     * @param age              {int}   Current age.
     **!/
    clc.ssIndexEarningFtr = function (wgGrw, inf, curYrsIndxErnFtr, age) {
        var val = 0,
            is67orOlder = (age >= 67),
            isBetween60to66 = (age >= 60) && (age <= 66);
        if (is67orOlder) {
            val = 0;
        } else if (isBetween60to66) {
            val = 1;
        } else {
            val = curYrsIndxErnFtr * (1 + wgGrw + inf);
        }
        return val;
    };
    /!***
     * Calculate the "social security index earnings" for the year.
     * @param {int} ssErn This years social security earnings
     * @param {int} ssIndxErnFtr This years social security indexed earning FTR.
     **!/
    clc.ssIndexEarnings = function (ssErn, ssIndxErnFtr) {
        return ssErn * ssIndxErnFtr;
    };
    /!***
     * Calculate "Begining of year social securtiy bend point A".
     * @param wgGrw       {float} Wage growth as decimal, not percent.
     * @param inf         {float} Inflation as decimal, not percent.
     * @param ssBptAy1    {float} Social security earnings on year 1.
     * @param yrsAccruing {float} Number of years ss bend points have been accruing.
     * Gotcha Note: because we calculate for "begining of year" think "base 0" where
     * if you want results for "year one" you pass in "0" for the param "yrsAccruing",
     * and for "year two" results, you'd pass in "1" for the param "yrsAccruing", etc.
     **!/
    clc.yearsSsBendPointA = function (wgGrw, inf, ssBptAy1, yrsAccruing) {
        var val = ssBptAy1 * Math.pow((1 + wgGrw + inf), yrsAccruing);
        return val;
    };
    /!***
     * Calculate "Begining of year social securtiy bend point B".
     * @param wgGrw       {float} Wage growth as decimal, not percent.
     * @param inf         {float} Inflation as decimal, not percent.
     * @param ssBptBy1    {float} Social security earnings on year 1.
     * @param yrsAccruing {float} Number of years ss bend points have been accruing.
     * Gotcha Note: because we calculate for "begining of year" think "base 0" where
     * if you want results for "year one" you pass in "0" for the param "yrsAccruing",
     * and for "year two" results, you'd pass in "1" for the param "yrsAccruing", etc.
     **!/
    clc.yearsSsBendPointB = function (wgGrw, inf, ssBptBy1, yrsAccruing) {
        var val = ssBptBy1 * Math.pow((1 + wgGrw + inf), yrsAccruing);
        return val;
    };
    /!***
     * Calculate "Discount percent" for the year as a decimal.
     * @param rateOr       {float} The assumed "Rate of return" as decimal, not percent.
     * @param yrsPastRtrm  {float} Years past retirement. On the year of retirement this value should be zero.
     **!/
    clc.discountPercentAsDecimal = function (rateOr, yrsPastRtrm) {
        var val = Math.pow((1 + rateOr), -yrsPastRtrm);
        return val;
    };
    /!***
     * Calculate "Discount value" for the year.
     * @param discPrcnt  {float} Social security "discount percent" for the year.
     * @param retmntInc  {float} Retirement income for the year.
     **!/
    clc.discountValue = function (discPrcnt, retmntInc) {
        return discPrcnt * retmntInc;
    };
    /!***
     * Calculate retirement income for the FIRST year of retirement.
     * @param replacementRate  {float} Replacement rate.
     * @param finalWrkYrsSlry  {float} Final work year's salary.
     **!/
    clc.retirementIncomeYear1 = function (replacementRate, finalWrkYrsSlry) {
        return finalWrkYrsSlry * replacementRate;
    };
    /!***
     * Calculate retirement income for the year.
     * @param inf              {float} Inflation.
     * @param prevYrRtrmntInc  {float} Previous years retirement income.
     **!/
    clc.retirementIncomeSubsequentYear = function (inf, prevYrRtrmntInc) {
        return prevYrRtrmntInc * (1 + inf);
    };
    /!***
     * Calculate monthly or annual social security payments at 67.
     * @param inf              {float} Inflation as a decimal.
     * @param pia              {float} Primary Insurance Amount.
     * @param annualOrMonthly  {string} Optional string indicating whether you want the 'annual' or 'monthly' payment ammount returned.
     **!/
    clc.ssPaymentAt67 = function (inf, pia, annualOrMonthly) {
        // Figure the monthly value to start.
        var val = pia * Math.pow((1 + inf), 5);
        // Round it to the nearest dollar.
        val = Math.floor(val);
        // Make this function default to returning the annual amount.
        annualOrMonthly = utl.defaultFor(annualOrMonthly, 'annual');
        if (annualOrMonthly === 'annual') {
            val = 12 * val;
        }
        return val;
    };
    /!***
     * Calculate social security payment any year after 67.
     * @param inf  {float} Inflation as a decimal.
     * @param ssPaymentPrevYr  {float} Previous years social security payment.Ï
     * @param savingsAcum  Previous years accrued social security savings.
     **!/
    clc.ssPaymentAfter67 = function (inf, ssPaymentPrevYr, savingsAcum) {
        //            val * (1 + 0.023);
        return ssPaymentPrevYr * (1 + inf);
    };
    /!***
     * Calculate delayed social security payment at age 67.
     * @param inf  {float} Inflation as a decimal.
     * @param ssPaymentPrevYr  {float} Previous years social security payment.
     * @param savingsAcum  Previous years accrued social security savings.
     **!/
    clc.ssPaymentDelayedFirstYearAfter67 = function (inf, ssPaymentPrevYr, savingsAcum) {
        var ssAmountDuringDelayedRetirement = ssPaymentPrevYr + savingsAcum;
        return ssAmountDuringDelayedRetirement;
    };
    /!***
     * Calculate delayed social security payment on year after 67.
     * @param ssSavingsDelayed_a previous years delayed SS savings.
     * @param ssPaymentPrevYr  {float} Previous years social security payment.
     * @param rorPerctage  Rate of return value.
     * @param slrySvns     .
     **!/
    clc.ssPaymentDelayedYearAfter68 = function (ssSavingsDelayed_a, ssPaymentPrevYr, slrySvns, rorPerctage) {
//            var ssAmountDuringDelayedRetirement = (ssSavingsDelayed_a * rorPerctage) + ssPaymentPrevYr + slrySvns;
        var ssAmountDuringDelayedRetirement = ssSavingsDelayed_a * (.06 + 1) + ssPaymentPrevYr + slrySvns;
//            console.log();

        return ssAmountDuringDelayedRetirement;
    };
    /!***
     * Calculate social security payment w/ discount for the year.
     * @param ssPayment                 {float} The social security .
     * @param discountPercentAsDecimal  {float} Discount percent as a decimal.
     **!/
    clc.ssPaymentWithDiscount = function (ssPayment, discountPercentAsDecimal) {
        return ssPayment * discountPercentAsDecimal;
    };
    /!***
     * Calculate AIME: Average Indexed Monthly Earnings
     * See http://en.wikipedia.org/wiki/Average_Indexed_Monthly_Earnings
     * See http://www.ssa.gov/oact/cola/Benefits.html#aime
     * @param indxErnsArray  {array} Array of indexed earnings for all years of employment.
     **!/
    clc.averageIndexedMonthlyEarnings = function (indxErnsArray) {
        var val = 0,
            sumOfHighest35 = 0,
            i = 0,
            a = 0,
            b = 0;
        // Sort highest to lowest.
        indxErnsArray.sort(function (a, b) {
            return b - a;
        });
        // Chop off max 35 salaries.
        if (indxErnsArray.length > 35) {
            indxErnsArray = indxErnsArray.slice(0, 35);
        }
        // Add them up.
        for (i = 0; i < indxErnsArray.length; i++) {
            sumOfHighest35 += indxErnsArray[i];
        }
        // Get the average of the 35 annual values as a monthly value.
        val = sumOfHighest35 / (35 * 12);
        return val;
    };
    /!***
     * Calculate PIA: "Primary Insurance Amount"
     * See http://en.wikipedia.org/wiki/Primary_Insurance_Amount
     * See http://www.ssa.gov/oact/cola/piaformula.html
     * @param bpt_a  {float} Bend point A.
     * @param bpt_b  {float} Bend point B.
     * @param amie   {float} Average Indexed Monthly Earnings.
     **!/
    clc.primaryInsuranceAmount = function (bpt_a, bpt_b, amie) {
        var val = 0,
            prcnt90ofBendA = 0.9 * Math.min(bpt_a, amie),
            prcnt32ofExcessUpToBptB = 0.32 * Math.max(Math.min(bpt_b - bpt_a, amie - bpt_a), 0),
            prcnt15ofExcessOverBend = 0.15 * Math.max(amie - bpt_b, 0);
        val = prcnt90ofBendA + prcnt32ofExcessUpToBptB + prcnt15ofExcessOverBend;
        val = Math.floor(val * 10) / 10; // Round to nearest 10th.
        //console.log('primaryInsuranceAmount');
        return val;
    };
    /!***
     * Calculate the total amount of money you will have upon retirement.
     * @param inf                      {float} The rate of inflation as a decimal point.
     * @param yearsNowToRtrmnt         {int} The number of years between this year (today) and retirement.
     * @param svngsUponRtrmnt          {float} Total amount saved upon retirement.
     * @param todayOrTomorrowDlrs      {string} Expects 1 of two strings indicating
     *   whether to adjust for inflation to reflect todays or tommorow dollars (the
     *   forecasted value of the dollar upon the person's year of retirement).
     **!/
    clc.willHave = function (inf, yearsNowToRtrmnt, svngsUponRtrmnt, todayOrTomorrowDlrs) {
        var val = svngsUponRtrmnt;
        if (todayOrTomorrowDlrs === 'today') {
            val = val * Math.pow((1 + inf), -yearsNowToRtrmnt);
        }
        // Round to nearest thousand dollars.
        val = Math.round(val / 1000) * 1000;
        return val;
    };
    /!***
     * Calculate the total amount of money you will need upon retirement.
     * @param inf                        {float} The rate of inflation as a decimal point.
     * @param yearsNowToRtrmnt           {int} The number of years between this year (today) and retirement.
     * @param eolSumOfDiscountedValues         {float} The end-of-life sum of all past years "discounted values".
     * @param eolSumOfSsPaymentsWithDiscount   {float} The end-of-life sum of all past years "ss payments with discount applied".
     * @param todayOrTomorrowDlrs              {string} Expects 1 of two strings indicating
     *   whether to adjust for inflation to reflect todays or tommorow dollars (the
     *   forecasted value of the dollar upon the person's year of retirement).
     **!/
//         not used: * @param svngsUponRtrmnt            {float} Total amount saved upon retirement.
    clc.willNeed = function (inf, yearsNowToRtrmnt, eolSumOfDiscountedValues, eolSumOfSsPaymentsWithDiscount, todayOrTomorrowDlrs) {
        var val = eolSumOfDiscountedValues - eolSumOfSsPaymentsWithDiscount;
        if (todayOrTomorrowDlrs === 'today') {
            val = val * Math.pow((1 + inf), -yearsNowToRtrmnt);
        }
        // Round to nearest thousand dollars.
        val = Math.round(val / 1000) * 1000;
        return val;
    };
    /!**
     * A function to determine which type of year we are working with when looping through
     * each year in the retirement plan scenario. Allows us to pull alot of the boolean
     * logic clutter out of the getAllYears() function.
     * @param ageStart   {int} The age at which the person is/was on the first year of all years tracked.
     * @param ageRetire  {int} The age at which the person plans to retire.
     * @param ageNow     {int} The age which the person is now, during the year in question.
     * @param ageEol     {int} The assumed max 'end of life' age the person will reach.
     * @return           {obj} An object with many true/false properties indicating what type of year
     *                         this is or is not.
     **!/
    clc.yearIs = function (ageStart, ageRetire, ageNow, ageEol) {
        return {
            /!** all **!/
            yr1: (ageNow === ageStart),
            yrAfter1: (ageNow > ageStart),
            /!** 67 **!/
            yrUnder67: (ageNow < 67),
            yr66: (ageNow === 66),
            yr67: (ageNow === 67),
            yr67OrOver: (ageNow >= 67),
            yrOver67: (ageNow > 67),
            /!** work **!/
            workYrAny: (ageNow <= ageRetire),
            workYr1: (ageNow === ageStart) && (ageStart <= ageRetire),
            workYrAfter1: (ageNow > ageStart) && (ageNow <= ageRetire),
            workYrNotLast: (ageNow < ageRetire),
            workYr1BeforeLast: (ageNow === (ageRetire - 1)),
            workYrLast: (ageNow === ageRetire),
            /!** retirement **!/
            retireYrAny: (ageNow >= (ageRetire + 1)),
            retireYr1: (ageNow === (ageRetire + 1)),
            retireYrAfter1: (ageNow > (ageRetire + 1)),
            retireYrNotLast: (ageNow >= (ageRetire + 1)) && (ageNow < ageEol),
            retireYrLast: (ageNow === ageEol),
            /!** early & delayed **!/
            pre67NonWorkYrDuringEarlyRetire: (ageRetire < 67) && (ageNow < 67) && (ageNow > ageRetire),
            post67ExtraWorkYrDuringDelayedRetire: (ageRetire > 67) && (ageNow > 67) && (ageNow < ageRetire)
        };
    };
    /!***
     * Calculate the additional years of work required in early retirement neccary for the AIME array.
     * @param ssSavingsAIMEAmount      {int} The additional years required for a full 35 in early retirement.
     *   --.
     **!/
//        function totalAIMEYears(ssSavingsAIMEAmount) {
//            console.log(':::::::ssSavingsAIMEAmount: ' + ssSavingsAIMEAmount);
//            var val = 0;
//            return function (x) {
//                val++;
//            };
//        }



///////////////////////////////////////////////////////====///////////////////////////////////////////////////////


    /!***
     * This main workhorse function which builds a table of "years data" that reflects
     * the "proof.xls" spread sheet. Note that the calculations are very procedural, like
     * russian dolls in that certain calculations can only be done after the results of
     * many subsequent initial calculation become available.
     **!/
    $scope.getAllYears = function () {
        /!** Two sets of iterator vars for our inner & outter loops **!/
        var i = 0,
            iMax = 0,
            ii = 0,
            iiMin = 0,
            /!** Cache inputs **!/
            inpAge = parseInt($scope.inputs.ageNow, 10),
            inpAgeRetire = parseInt($scope.inputs.ageRetire, 10),
            inpSlry = parseInt($scope.inputs.salary, 10),
            inpSvnBase = parseInt($scope.inputs.savingsBase, 10),
            inpSvngRate = utl.percentToDecimal($scope.inputs.savingsRateAsPercent),
            inpWrkYrsCnt = $scope.inputs.getCountOfWorkYears(),
            inpRetireDate = $scope.inputs.getDateOfRetire(),
            /!** Cache assumptions **!/
            asmInf = $scope.asm.inf,
            asmWgGrw = $scope.asm.wgGrw,
            asmRepRate = $scope.asm.repRate,
            asmRateOr = $scope.asm.rateOr,
            asmEolAge = $scope.asm.eolAge,
            /!** Cach ss factors **!/
            ssContribBase = $scope.ssf.contribBase,
            ssBptA = $scope.ssf.bptA,
            ssBptB = $scope.ssf.bptB,
            // Cach display settings **!/
            strNa = $scope.display.naVal,
            /!** Vars to store "year data" as we loop through each year and collect "all year data" into the "allYrs" array. **!/
            allYrs = [],
            lastYr = false,
            /!** NOTE: The following "dedictedIndexedEarningsArray" is a variable to store
             'indexed earnings' that we will calculate on a inner loop that fires at
             the age/year of 67 and reverse/loops back through the years to gather
             then calculate the required info. **!/
            dedictedIndexedEarningsArray = [],
            /!** A few variables to store final "umbrella" calculations which require
             that we look back at all collected year data in order to detrmine
             their value. **!/
            finalTotalOfYrlyDiscntValsAtEol = 0,
            finalTotalOfSsPaymentsWithDiscount = 0,
            finalSvngsUponRtrmntInTmrwDlrs = 0,
            /!** We initialize our curYear[FOO] vals here so they reflect the first year.
             After loop one, for all subsequent years, these variables are calculated
             a bit differently. **!/
            curYrIs = clc.yearIs(inpAge, inpAgeRetire, inpAge, asmEolAge),
            curYrAge = inpAge,
            /!** Initialize pre retire values **!/
            curYrSlry = curYrIs.retireYrAfter1 ? strNa : inpSlry,
            curYrSlrySvns = curYrIs.retireYrAny ? strNa : clc.yearsSavingsFromSalary(inpSvngRate, curYrSlry),
            curYrSvnsAcumEoy = curYrIs.retireYrAny ? strNa : clc.yearsAccumulatedSavings(asmInf, asmRateOr, inpSvnBase, 0) + curYrSlrySvns,
            curYrSsEarn = curYrIs.yr67OrOver ? strNa : clc.ssEarningsYear1(curYrSlry, ssContribBase),
            curYrSsBpA = curYrIs.yr67OrOver ? strNa : clc.yearsSsBendPointA(asmWgGrw, asmInf, ssBptA, i),
            curYrSsBpB = curYrIs.yr67OrOver ? strNa : clc.yearsSsBendPointB(asmWgGrw, asmInf, ssBptB, i),
            /!** Initialize post retire values **!/
            curYrRtrmntInc = strNa,
            isCollapsed = true; // ...for now assuming year1 will never be a retirement year.
        curYrDiscPcnt = curYrIs.workYrNotLast ? strNa : clc.discountPercentAsDecimal(asmRateOr, curYrAge - inpAgeRetire),
            curYrDiscVal = strNa, // ...for now assuming year1 will never be a retirement year.
            curYrYr = new Date().getFullYear(),
            curYrYrsAccrued = 0,
            curYrYrsPast67 = inpAge < 67 ? strNa : clc.yrsPast67(inpAge),
            curYrSsPymnt = strNa,
            curYrSsPymntWDisc = strNa,
            curYrSsIdxErnFtr = strNa,
            curYrSsIdxErn = strNa,
            /!** Stash this value for out subsequent years/loops to use in calculations **!/
            firstYrSsEarn = curYrSsEarn;
        /!** Begin looping through the years.
         For each year between now and EOL. **!/
        var ssSavingsDelayed_1 = 'undefinded';
        var ssSavingsDelayed_2 = 'undefinded';
        var extraYears = [0];
        for (i = 0, iMax = asmEolAge - inpAge; i <= iMax; i++) {
            /!** If this is not year 1 (for which we instantiate the above variables for already)....**!/
            if (lastYr !== false) {
                /!** ...then update all our variables to reflect this subsequent year. **!/
                curYrIs = clc.yearIs(inpAge, inpAgeRetire, inpAge + i, asmEolAge);
                curYrAge = lastYr.age + 1;
                /!** Update pre retire values **!/
                if (curYrIs.workYrNotLast) {
                    curYrSlry = clc.nextYrsSalary(asmWgGrw, asmInf, lastYr.slry);
                } else if (curYrIs.workYrLast) {
                    curYrSlry = clc.nextYrsSalary(0, asmInf, lastYr.slry); // asmWgGrw is 0 for last working year.
                } else {
                    curYrSlry = strNa;
                }
                curYrSlrySvns = curYrIs.retireYrAny ? strNa : clc.yearsSavingsFromSalary(inpSvngRate, curYrSlry);
                curYrSvnsAcumEoy = curYrIs.retireYrAny ? strNa : clc.yearsAccumulatedSavings(asmInf, asmRateOr, lastYr.svns_acum_eoy, curYrSlrySvns);
                curYrSsEarn = curYrIs.yr67OrOver ? strNa : clc.ssEarningsSubsequentYears(asmWgGrw, asmInf, firstYrSsEarn, i, curYrAge, inpAgeRetire);
                curYrSsBpA = curYrIs.yr67OrOver ? strNa : clc.yearsSsBendPointA(asmWgGrw, asmInf, ssBptA, i);
                curYrSsBpB = curYrIs.yr67OrOver ? strNa : clc.yearsSsBendPointB(asmWgGrw, asmInf, ssBptB, i);
                /!** Update the post retire values **!/
                if (curYrIs.workYrLast) {
                    curYrRtrmntInc = clc.retirementIncomeYear1(asmRepRate, curYrSlry);
                } else if (curYrIs.retireYrAny) {
                    curYrRtrmntInc = clc.retirementIncomeSubsequentYear(asmInf, lastYr.rtm_inc);
                } else {
                    curYrRtrmntInc = strNa;
                }
                curYrDiscPcnt = curYrIs.workYrNotLast ? strNa : clc.discountPercentAsDecimal(asmRateOr, curYrAge - inpAgeRetire);
                curYrDiscVal = curYrIs.workYrNotLast ? strNa : clc.discountValue(curYrDiscPcnt, curYrRtrmntInc);
                curYrYr = lastYr.year + 1;
                curYrYrsAccrued = lastYr.yrs_accrued + 1;
                curYrYrsPast67 = curYrIs.yrUnder67 ? strNa : clc.yrsPast67(lastYr.age + 1);
            } // end if

            /!** ----------------------------------------
             If this is a STANDARD retirment year
             ----------------------------------------- **!/
            if (curYrIs.yr67) {
//                    /!** At age 67 we can now calculate "Indexed Earing" data by
//                     looping back through all work year data previous to age 66. **!/
                for (ii = allYrs.length - 1, iiMin = 0; ii >= iiMin; ii--) {
//                        /!** At age 62 record "bend points" for this standard "age of eligibility".**!/
                    if (allYrs[ii].age === 62) {
                        $scope.output.bptA = Math.round(allYrs[ii].ss_bp_a);
                        $scope.output.bptB = Math.round(allYrs[ii].ss_bp_b);
                    }
//                        /!** At age 66 calculate our initial "IndexEarningsFTR" as "1" **!/
                    if (allYrs[ii].age === 66) {
                        allYrs[ii].ss_idx_ern_ftr = 1;
                    } else {
                        /!** The for all prior years we loop back through calculate "IndexEarningsFTR" for them
                         based on the CHRONOLOGICALLY-NEXT years (PREVIOUS in our loop) value for IndexEarningsFTR. **!/
                        allYrs[ii].ss_idx_ern_ftr = clc.ssIndexEarningFtr(asmWgGrw, asmInf, allYrs[ii + 1].ss_idx_ern_ftr, allYrs[ii].age);
                    }
//                        /!** Now calculate the actual IndexEarnings from the IndexEarningsFTR and add it to our dedicated array **!/
                    allYrs[ii].ss_idx_ern = clc.ssIndexEarnings(allYrs[ii].ss_earn, allYrs[ii].ss_idx_ern_ftr);
                    if (isNaN(allYrs[ii].ss_idx_ern) || allYrs[ii].ss_idx_ern === 0) {
                        extraYears.push(0);
                    } else {
                        dedictedIndexedEarningsArray.push(allYrs[ii].ss_idx_ern);
                    }
                }
                for (var j = 0, k = extraYears.length; j < k; j++) {
                    dedictedIndexedEarningsArray.push(allYrs[j].ss_idx_ern);
                }
//                    /!** Now that all IndexedEarning data is gathered, we can calculate AIME & PIA,
//                     two of our main/final retirement factors needed to determine other key final values. **!/
                $scope.output.aime = clc.averageIndexedMonthlyEarnings(dedictedIndexedEarningsArray);
                $scope.output.pia = clc.primaryInsuranceAmount($scope.output.bptA, $scope.output.bptB, $scope.output.aime);
                // Note: We capture both 'annual' & 'monthly' benifits for display & calculation purposes.
                $scope.output.ssMnthBenAt67 = clc.ssPaymentAt67(asmInf, $scope.output.pia, 'monthly');
                curYrSsPymnt = clc.ssPaymentAt67(asmInf, $scope.output.pia, 'annual');
                curYrSsPymntWDisc = clc.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
                ssSavingsDelayed_1 = curYrSsPymnt + curYrSvnsAcumEoy;
                ssSavingsDelayed_2 = curYrSsPymnt;
            }

            /!** 1 Year Before Retirement Year **!/
            if (curYrIs.workYr1BeforeLast) {
                finalSvngsUponRtrmntInTmrwDlrs = curYrSvnsAcumEoy;
            }

            /!** --------------------------------------
             If this is a DELAYED retirement year
             ----------------------------------------- **!/
            if (curYrIs.post67ExtraWorkYrDuringDelayedRetire) {
//                    finalSvngsUponRtrmntInTmrwDlrs = (lastYr.svns_acum_eoy + ssSavingsDelayed_2);
                if (curYrAge <= inpAgeRetire) {
//                       :: the below needs to be cyphered before the the rest of this conditional ::
                    ssSavingsDelayed_1 = (ssSavingsDelayed_1 * 1.06) + (curYrSlrySvns + (curYrSsPymnt * 1.023));
                    ssSavingsDelayed_2 = (ssSavingsDelayed_2 * 1.06) + (curYrSsPymnt * 1.023);
//                       :: the above needs to be cyphered before the the rest of this conditional ::
                }
                curYrSsPymnt = curYrSsPymnt * 1.023;
                curYrSsPymntWDisc = clc.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
            }
            /!** --------------------------------------
             This is the first retirement year
             ----------------------------------------- **!/
            if (curYrAge === inpAgeRetire) {
                /!** At age 67 we can now calculate "Indexed Earing" data by
                 looping back through all work year data previous to age 66. **!/
                for (iii = allYrs.length - 1, iiiMin = 0; iii >= iiMin; iii--) {
                    /!** At age 62 record "bend points" for this standard "age of eligibility".**!/
                    if (allYrs[iii].age === 62) {
                        $scope.output.bptA = Math.round(allYrs[iii].ss_bp_a);
                        $scope.output.bptB = Math.round(allYrs[iii].ss_bp_b);
                    }
                    /!** At retirement age calculate our initial "IndexEarningsFTR" as "1" **!/
                    if (allYrs[iii].age === inpAgeRetire - 1) {
                        allYrs[iii].ss_idx_ern_ftr = 1;
                    } else {
                        /!** The for all prior years we loop back through calculate "IndexEarningsFTR" for them
                         based on the CHRONOLOGICALLY-NEXT years (PREVIOUS in our loop) value for IndexEarningsFTR. **!/
                        allYrs[iii].ss_idx_ern_ftr = clc.ssIndexEarningFtr(asmWgGrw, asmInf, allYrs[iii + 1].ss_idx_ern_ftr, allYrs[iii].age);
                    }
                }
            }
            /!** --------------------------------------
             Calculate SS payments for any year following the age of retirement
             ----------------------------------------- **!/
            if ((curYrAge + 1) > inpAgeRetire && curYrAge !== 67) {
                curYrSsPymnt = curYrSsPymnt * 1.023;
                curYrSsPymntWDisc = clc.ssPaymentWithDiscount(curYrSsPymnt, curYrDiscPcnt);
            }
            /!** --------------------------------------
             If this is the first DELAYED retirement year where we adjust the 'you will have' total to include additional late retirement savings.
             ----------------------------------------- **!/
            if (curYrAge === inpAgeRetire && curYrAge > 67) {
                finalSvngsUponRtrmntInTmrwDlrs = lastYr.svns_acum_eoy + ssSavingsDelayed_2;
            }
            /!** --------------------------------------
             This year's is now collected. Add it to our array.
             ----------------------------------------- **!/
            allYrs[i] = {
                year: curYrYr,
                yrs_accrued: curYrYrsAccrued,
                yrs_past67: curYrYrsPast67,
                age: curYrAge,
                rtm_inc: curYrRtrmntInc,
                slry: curYrSlry,
                slry_svns: curYrSlrySvns,
                svns_acum_eoy: curYrSvnsAcumEoy,
                disc_pcnt: curYrDiscPcnt,
                disc_val: curYrDiscVal,
                ss_earn: curYrSsEarn,
                ss_bp_a: curYrSsBpA,
                ss_bp_b: curYrSsBpB,
                ss_pymnt: curYrSsPymnt,
                ss_pymnt_wdisc: curYrSsPymntWDisc,
                ss_idx_ern_ftr: curYrSsIdxErnFtr,
                ss_idx_ern: curYrSsIdxErn,
                delayedRetire_1: ssSavingsDelayed_1,
                delayedRetire_2: ssSavingsDelayed_2
            };
            /!** Prep for the next loop. This-year now becomes last-year. **!/
            lastYr = allYrs[i];
        } // End outermost loop

        /!** Now that we have all year data stored in allYrs[]
         we can calculate our final numbers.**!/

        /!** Reuse "i" as we are finished using it in the above loop. **!/
        for (i = 0, iMax = allYrs.length - 1; i <= iMax; i++) {
            /!** Get the totals needed to calculate other values  **!/
            finalTotalOfYrlyDiscntValsAtEol += isNaN(allYrs[i].disc_val) ? 0 : allYrs[i].disc_val;
            finalTotalOfSsPaymentsWithDiscount += isNaN(allYrs[i].ss_pymnt_wdisc) ? 0 : allYrs[i].ss_pymnt_wdisc;
        }
        /!** Display our key totals **!/
        $scope.output.totalSvngsOnRtrmnt = clc.willHave(asmInf, inpWrkYrsCnt, finalSvngsUponRtrmntInTmrwDlrs, 'tomorrow');
        $scope.output.totalOfYrlDiscntValsAtEol = finalTotalOfYrlyDiscntValsAtEol;
        /!** Calculate the amount they will have upon retirement. **!/
        $scope.output.willHave = clc.willHave(asmInf, inpWrkYrsCnt, $scope.output.totalSvngsOnRtrmnt, 'tomorrow');
        $scope.output.willHaveTdyDlr = clc.willHave(asmInf, inpWrkYrsCnt, $scope.output.totalSvngsOnRtrmnt, 'today');
        /!** Calculate the ammount they will need upon retirement. **!/
        $scope.output.willNeed = clc.willNeed(asmInf, inpWrkYrsCnt, finalTotalOfYrlyDiscntValsAtEol, finalTotalOfSsPaymentsWithDiscount, 'tomorrow');
        $scope.output.willNeedTdyDlr = clc.willNeed(asmInf, inpWrkYrsCnt, finalTotalOfYrlyDiscntValsAtEol, finalTotalOfSsPaymentsWithDiscount, 'today');
        /!** All year gathering and subsequent computations are now done.
         Return our years array for display in the table **!/
        return allYrs;
    };
    // end getAllYears();

    /!** TESTS
     * All tests are based on values in proof.xls.
     ---------------------- **!/
    test = {};
    /!***
     * A function to run all tests during development to assure changes to code
     * break existing functionality. The call to test.runAll() can be uncommented
     * during dev, and is currently placed within the main updateDisplay() function.
     **!/
    test.runAll = function () {
        test.nextYrsSalary();
        test.yearsSavingsFromSalary();
        test.yearsAccumulatedSavings();
        test.yearsSsBendPointA();
        test.yearsSsBendPointB();
        test.ssEarningsYear1();
        test.ssEarningsSubsequentYears();
        test.ssIndexEarningFtr();
        test.ssIndexEarnings();
        test.ssPaymentAt67();
        test.ssPaymentAfter67();
        test.ssPaymentWithDiscount();
        test.discountPercentAsDecimal();
        test.discountValue();
        test.retirementIncomeYear1();
        test.retirementIncome();
        test.primaryInsuranceAmount();
        test.averageIndexedMonthlyEarnings();
        test.willHave();
        test.willNeed();
        test.yearIs();
        test.yrsPast67();
        test.getXPercentOfY();
        test.xIsWhatPercentOfY();
        test.allFinalOutputsForAge30Retire67(false);
    };
    test.getXPercentOfY = function () {
        expectedA = 100,
            expectedB = 750,
            resultA = clc.getXPercentOfY(10, 1000),
            resultB = clc.getXPercentOfY(75, 1000),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('getXPercentOfY() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('getXPercentOfY() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.xIsWhatPercentOfY = function () {
        expectedA = 20,
            expectedB = 75,
            resultA = clc.xIsWhatPercentOfY(200, 1000),
            resultB = clc.xIsWhatPercentOfY(750, 1000),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('xIsWhatPercentOfY() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('xIsWhatPercentOfY() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.yearIs = function () {
        //console.log('------------------------- \n   TESTNOTE: test.yearIs()" implimentation is TBD. \n ------------------------- ');
    };
    test.willHave = function () {
        var inf = 0.023,
            yearsNowToRtrmnt = 37,
            svngsUponRtrmntInTmrwDlrs = 594074.1011692,
            expectedA = 256000,
            expectedB = 594000,
            resultA = clc.willHave(inf, yearsNowToRtrmnt, svngsUponRtrmntInTmrwDlrs, 'today'),
            resultB = clc.willHave(inf, yearsNowToRtrmnt, svngsUponRtrmntInTmrwDlrs, 'tomorrow'),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('willHave() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('willHave() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.willNeed = function () {
        var inf = 0.023,
            yearsNowToRtrmnt = 37,
            eolSumOfDiscountedValuesInTmrwDlrs = 14954731.930803,
            eolSumOfSsPaymentsWithDiscountInTmrwDlrs = 2067967.386088,
            expectedB = 5556000,
            expectedA = 12887000,
            resultA = clc.willNeed(inf, yearsNowToRtrmnt, eolSumOfDiscountedValuesInTmrwDlrs, eolSumOfSsPaymentsWithDiscountInTmrwDlrs, 'tomorrow'),
            resultB = clc.willNeed(inf, yearsNowToRtrmnt, eolSumOfDiscountedValuesInTmrwDlrs, eolSumOfSsPaymentsWithDiscountInTmrwDlrs, 'today');
        passedA = (expectedA === resultA);
        passedB = (expectedB === resultB);
        if (!passedA)
            alert('willNeed() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('willNeed() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.primaryInsuranceAmount = function () {
        var bpt_a = 2691.00,
            bpt_b = 16220.00,
            expectedA = 8904.20,
            expectedB = 8800.30,
            resultA = clc.primaryInsuranceAmount(2691.00, 16220.00, 30574.01),
            resultB = clc.primaryInsuranceAmount(bpt_a, bpt_b, 29881.09),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('primaryInsuranceAmount() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('primaryInsuranceAmount() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.averageIndexedMonthlyEarnings = function () {
        var indxErnsA = [358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 358184.24, 371795.24, 385923.45, 400588.55, 415810.91, 431611.73, 448012.97],
            indxErnsB = [358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 358184.2350629990, 371795.2359953930, 385923.4549632180, 400588.5462518200, 415810.9110093890, 431611.7256277460, 448012.9712016000],
            expectedA = 30574.01,
            expectedB = 30574.01,
            resultA = clc.averageIndexedMonthlyEarnings(indxErnsA),
            resultB = clc.averageIndexedMonthlyEarnings(indxErnsB),
            resultA = utl.roundToHundreds(resultA),
            resultB = utl.roundToHundreds(resultB),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('averageIndexedMonthlyEarnings() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('averageIndexedMonthlyEarnings() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.ssPaymentAfter67 = function () {
        var inf = 0.023,
            expectedA = 122465.38,
            expectedB = 176207.12,
            resultA = clc.ssPaymentAfter67(inf, 119712.00),
            resultB = clc.ssPaymentAfter67(inf, 172245.47673715),
            resultA = utl.roundToHundreds(resultA),
            resultB = utl.roundToHundreds(resultB),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('ssPaymentAfter67() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('ssPaymentAfter67() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.ssPaymentWithDiscount = function () {
        var ssPayment = 122465.3760,
            discountPercentAsDecimal = 0.94339623,
            expectedA = 115533.37,
            resultA = clc.ssPaymentWithDiscount(ssPayment, discountPercentAsDecimal),
            resultA = utl.roundToHundreds(resultA),
            passedA = (expectedA === resultA);
        if (!passedA)
            alert('ssPaymentWithDiscount() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
    };
    test.ssPaymentAt67 = function () {
        var inf = 0.023,
            pia = 8904.20,
            expectedA = 9976.00,
            expectedB = 9976.00 * 12,
            resultA = clc.ssPaymentAt67(inf, pia, 'monthly'),
            resultB = clc.ssPaymentAt67(inf, pia, 'annual'),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('ssPaymentAt67() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('ssPaymentAt67() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.retirementIncomeYear1 = function () {
        var replacementRate = .85,
            expectedA = 865710.40,
            expectedB = 1405847.55,
            resultA = clc.retirementIncomeYear1(replacementRate, 1018482.821198),
            resultB = clc.retirementIncomeYear1(replacementRate, 1653938.298420),
            resultA = utl.roundToHundreds(resultA),
            resultB = utl.roundToHundreds(resultB),
            passedA = (expectedA === resultA),
            passedB = (expectedB === resultB);
        if (!passedA)
            alert('retirementIncomeYear1() \nexpectedA: ' + expectedA + '\nresultA:      ' + resultA);
        if (!passedB)
            alert('retirementIncomeYear1() \nexpectedB: ' + expectedB + '\nresultB:      ' + resultB);
    };
    test.retirementIncome = function () {
        var inf = 0.023,
            expected2yr = 885621.74,
            expected4yr = 948145.89,
            result2yr = clc.retirementIncomeSubsequentYear(inf, 865710.398019),
            result4yr = clc.retirementIncomeSubsequentYear(inf, 926828.830982),
            result2yr = utl.roundToHundreds(result2yr),
            result4yr = utl.roundToHundreds(result4yr),
            passed2yr = (expected2yr === result2yr),
            passed4yr = (expected4yr === result4yr);
        if (!passed2yr)
            alert('retirementIncome() \nexpected2yr: ' + expected2yr + '\nresult2yr:      ' + result2yr);
        if (!passed4yr)
            alert('retirementIncome() \nexpected4yr: ' + expected4yr + '\nresult4yr:      ' + result4yr);
    };
    test.discountPercentAsDecimal = function () {
        var rateOr = 0.06,
            expected1yr = utl.percentToDecimal(100),
            expected4yr = utl.percentToDecimal(79),
            result1yr = clc.discountPercentAsDecimal(rateOr, 0),
            result4yr = clc.discountPercentAsDecimal(rateOr, 4),
            result1yr = utl.roundToHundreds(result1yr),
            result4yr = utl.roundToHundreds(result4yr),
            passed1yr = (expected1yr === result1yr),
            passed4yr = (expected4yr === result4yr);
        if (!passed1yr)
            alert('discountPercentAsDecimal() \nexpected1yr: ' + expected1yr + '\nresult1yr:      ' + result1yr);
        if (!passed4yr)
            alert('discountPercentAsDecimal() \nexpected4yr: ' + expected4yr + '\nresult4yr:      ' + result4yr);
    };
    test.discountValue = function () {
        var expected2yr = 1309416.37,
            expected4yr = 1219599.68,
            result2yr = clc.discountValue(0.8899964400, 1471260.23),
            result4yr = clc.discountValue(0.7920936632, 1539716.50),
            result2yr = utl.roundToHundreds(result2yr),
            result4yr = utl.roundToHundreds(result4yr),
            passed2yr = (expected2yr === result2yr),
            passed4yr = (expected4yr === result4yr);
        if (!passed2yr) {
            alert('discountValue() \nexpected2yr: ' + expected2yr + '\nresult2yr:      ' + result2yr);
        }
        if (!passed4yr) {
            alert('discountValue() \nexpected4yr: ' + expected4yr + '\nresult4yr:      ' + result4yr);
        }
    };
    test.yearsSsBendPointB = function () {
        var wgGrw = utl.percentToDecimal(1.5),
            inf = utl.percentToDecimal(2.3),
            ssBptBy1 = 4917.36,
            expected1yr = 4917.36,
            expected2yr = 5104.22,
            expected37yr = 18829.41,
            result1yr = clc.yearsSsBendPointB(wgGrw, inf, ssBptBy1, 0),
            result2yr = clc.yearsSsBendPointB(wgGrw, inf, ssBptBy1, 1),
            result37yr = clc.yearsSsBendPointB(wgGrw, inf, ssBptBy1, 36),
            result1yr = utl.roundToHundreds(result1yr),
            result2yr = utl.roundToHundreds(result2yr),
            result37yr = utl.roundToHundreds(result37yr),
            passed1yr = (expected1yr === result1yr),
            passed2yr = (expected2yr === result2yr),
            passed37yr = (expected37yr === result37yr);
        if (!passed1yr)
            alert('yearsSsBendPointB() \nexpected1yr: ' + expected1yr + '\nresult1yr:      ' + result1yr);
        if (!passed2yr)
            alert('yearsSsBendPointB() \nexpected2yr: ' + expected2yr + '\nresult2yr:      ' + result2yr);
        if (!passed37yr)
            alert('yearsSsBendPointB() \nexpected37yr: ' + expected37yr + '\nresult37yr:      ' + result37yr);
    };
    test.yearsSsBendPointA = function () {
        var wgGrw = utl.percentToDecimal(1.5),
            inf = utl.percentToDecimal(2.3),
            ssBptAy1 = 815.78,
            expected1yr = 815.78,
            expected2yr = 846.78,
            expected37yr = 3123.76,
            result1yr = clc.yearsSsBendPointA(wgGrw, inf, ssBptAy1, 0),
            result2yr = clc.yearsSsBendPointA(wgGrw, inf, ssBptAy1, 1),
            result37yr = clc.yearsSsBendPointA(wgGrw, inf, ssBptAy1, 36),
            result1yr = utl.roundToHundreds(result1yr),
            result2yr = utl.roundToHundreds(result2yr),
            result37yr = utl.roundToHundreds(result37yr),
            passed1yr = (expected1yr === result1yr),
            passed2yr = (expected2yr === result2yr),
            passed37yr = (expected37yr === result37yr);
        if (!passed1yr)
            alert('yearsSsBendPointA() \nexpected1yr: ' + expected1yr + '\nresult1yr:      ' + result1yr);
        if (!passed2yr)
            alert('yearsSsBendPointA() \nexpected2yr: ' + expected2yr + '\nresult2yr:      ' + result2yr);
        if (!passed37yr)
            alert('yearsSsBendPointA() \nexpected37yr: ' + expected37yr + '\nresult37yr:      ' + result37yr);
    };
    test.ssIndexEarnings = function () {
        var expected1yr = 358184.24,
            expected2yr = 358184.24,
            result1yr = clc.ssIndexEarnings(117000, 3.061403718487170),
            result2yr = clc.ssIndexEarnings(121446.00, 2.949329208561820),
            result1yr = utl.roundToHundreds(result1yr),
            result2yr = utl.roundToHundreds(result2yr),
            passed1yr = (expected1yr === result1yr),
            passed2yr = (expected2yr === result2yr);
        if (!passed1yr)
            alert('ssIndexEarnings() \nexpected1yr: ' + expected1yr + '\nresult1yr:      ' + result1yr);
        if (!passed2yr)
            alert('ssIndexEarnings() \nexpected2yr: ' + expected2yr + '\nresult2yr:      ' + result2yr);
    };
    test.ssIndexEarningFtr = function () {
        var expected1yr = 3.06,
            expected15yr = 1.82,
            expected31yr = 1.00,
            expected38yr = 0,
            result1yr = clc.ssIndexEarningFtr(utl.percentToDecimal(1.5), utl.percentToDecimal(2.3), 2.95, 30),
            result15yr = clc.ssIndexEarningFtr(utl.percentToDecimal(1.5), utl.percentToDecimal(2.3), 1.75, 44),
            result31yr = clc.ssIndexEarningFtr(utl.percentToDecimal(1.5), utl.percentToDecimal(2.3), 1, 60),
            result38yr = clc.ssIndexEarningFtr(utl.percentToDecimal(1.5), utl.percentToDecimal(2.3), 0, 67),
            result1yr = utl.roundToHundreds(result1yr),
            result15yr = utl.roundToHundreds(result15yr),
            result31yr = utl.roundToHundreds(result31yr),
            result38yr = utl.roundToHundreds(result38yr),
            passed1yr = (expected1yr === result1yr),
            passed15yr = (expected15yr === result15yr),
            passed31yr = (expected31yr === result31yr),
            passed38yr = (expected38yr === result38yr);
        if (!passed1yr)
            alert('ssIndexEarningFtr() \nexpected1yr: ' + expected1yr + '\nresult1yr: ' + result1yr);
        if (!passed15yr)
            alert('ssIndexEarningFtr() \nexpected15yr: ' + expected15yr + '\nresult15yr: ' + result15yr);
        if (!passed31yr)
            alert('ssIndexEarningFtr() \nexpected31yr: ' + expected31yr + '\nresult31yr: ' + result31yr);
        if (!passed38yr)
            alert('ssIndexEarningFtr() \nexpected38yr: ' + expected38yr + '\nresult38yr: ' + result38yr);
    };
    test.ssEarningsSubsequentYears = function () {
        var wgGrw = utl.percentToDecimal(1.5),
            inf = utl.percentToDecimal(2.3),
            expected2yr = 121446,
            expected15yr = 197219.03,
            expected38yr = 0,
            expectedXyr = 121446.00,
            expected25yrRetire55 = 61189.44,
            expected26yrRetire55 = 0,
            result2yr = clc.ssEarningsSubsequentYears(wgGrw, inf, 117000, 1, 31, 67),
            result15yr = clc.ssEarningsSubsequentYears(wgGrw, inf, 117000, 14, 44, 67),
            result38yr = clc.ssEarningsSubsequentYears(wgGrw, inf, 117000, 37, 67, 67),
            resultXyr = clc.ssEarningsSubsequentYears(wgGrw, inf, 117000, 1, 31, 67),
            result25yr = clc.ssEarningsSubsequentYears(wgGrw, inf, 25000, 24, 54, 55),
            result26yr = clc.ssEarningsSubsequentYears(wgGrw, inf, 25000, 25, 55, 55),
            result2yr = utl.roundToHundreds(result2yr),
            result15yr = utl.roundToHundreds(result15yr),
            result38yr = utl.roundToHundreds(result38yr),
            resultXyr = utl.roundToHundreds(resultXyr),
            result25yr = utl.roundToHundreds(result25yr),
            result26yr = utl.roundToHundreds(result26yr);
        passed2yr = (expected2yr === result2yr),
            passed15yr = (expected15yr === result15yr),
            passed38yr = (expected38yr === result38yr),
            passedXyr = (expectedXyr === resultXyr),
            passed25yr = (expected25yrRetire55 === result25yr),
            passed26yr = (expected26yrRetire55 === result26yr);
        if (!passed2yr)
            alert('ssEarningsSubsequentYears() \nexpected2yr: ' + expected2yr + ' \nresult2yr: ' + result2yr);
        if (!passed15yr)
            alert('ssEarningsSubsequentYears() \nexpected15yr: ' + expected15yr + ' \nresult15yr: ' + result15yr);
        if (!passed38yr)
            alert('ssEarningsSubsequentYears() \nexpected38yr: ' + expected38yr + ' \nresult38yr: ' + result38yr);
        if (!passedXyr)
            alert('ssEarningsSubsequentYears() \nexpectedXyr: ' + expectedXyr + ' \nresultXyr: ' + resultXyr);
        if (!passed25yr)
            alert('ssEarningsSubsequentYears() \nexpected25yrRetire55: ' + expected25yrRetire55 + '\nresult25yr: ' + result25yr);
        if (!passed26yr)
            alert('ssEarningsSubsequentYears() \nexpected26yrRetire55: ' + expected26yrRetire55 + '\nresult26yr: ' + result26yr);
    };
    test.ssEarningsYear1 = function () {
        var expected1yr = 117000,
            result1yr = clc.ssEarningsYear1(260000, 117000),
            passed1yr = (expected1yr === result1yr);
        if (!passed1yr)
            alert('One or more ssEarningsYear1() tests failed.');
    };
    test.yrsPast67 = function () {
        var expected64yr = 0,
            expected67yr = 0,
            expected70yr = 3,
            result64yr = clc.yrsPast67(64),
            result67yr = clc.yrsPast67(67),
            result70yr = clc.yrsPast67(70),
            passed64yr = (expected64yr === result64yr),
            passed67yr = (expected67yr === result67yr),
            passed70yr = (expected70yr === result70yr);
        if (!passed64yr)
            alert('yrsPast67() \nexpected64yr: ' + expected64yr + ' \nresult64yr: ' + result64yr);
        if (!passed67yr)
            alert('yrsPast67() \nexpected67yr: ' + expected67yr + ' \nresult67yr: ' + result67yr);
        if (!passed70yr)
            alert('yrsPast67() \nexpected70yr: ' + expected70yr + ' \nresult70yr: ' + result70yr);
    };
    test.yearsAccumulatedSavings = function () {
        var inf = utl.percentToDecimal(2.3);
        expected1Yr = 18000.00,
            expected2Yr = 21259.80,
            expected16Yr = 107231.24, // NOTE: changed from .25 to account for roudning difference.
            result1Yr = clc.yearsAccumulatedSavings(inf, utl.percentToDecimal(6), 15000, 2100),
            result2Yr = clc.yearsAccumulatedSavings(inf, utl.percentToDecimal(6), 18000, 2179.80),
            result16Yr = clc.yearsAccumulatedSavings(inf, utl.percentToDecimal(6), 97695.19, 3674.34),
            passed1yr = parseFloat(utl.roundToHundreds(result1Yr)) === parseFloat(expected1Yr),
            passed2yr = parseFloat(utl.roundToHundreds(result2Yr)) === parseFloat(expected2Yr),
            passed16yr = parseFloat(utl.roundToHundreds(result16Yr)) === parseFloat(expected16Yr),
            passed = (passed1yr && passed2yr && passed16yr) ? true : false;
        if (!passed)
            alert('One or more yearsAccumulatedSavings() tests failed.');
    };
    test.nextYrsSalary = function () {
        var wgGrw = utl.percentToDecimal(1.5),
            inf = utl.percentToDecimal(2.3),
            curSlry = 35000,
            expected1Yr = 36330.00,
            expected2Yr = 37710.54,
            expected16Yr = 63566.12,
            result1Yr = clc.nextYrsSalary(wgGrw, inf, curSlry),
            result2Yr = clc.nextYrsSalary(wgGrw, inf, 36330),
            result16Yr = clc.nextYrsSalary(wgGrw, inf, 61239.04),
            passed1yr = parseFloat(utl.roundToHundreds(result1Yr)) === parseFloat(expected1Yr),
            passed2yr = parseFloat(utl.roundToHundreds(result2Yr)) === parseFloat(expected2Yr),
            passed16yr = parseFloat(utl.roundToHundreds(result16Yr)) === parseFloat(expected16Yr),
            passed = (passed1yr && passed2yr && passed16yr) ? true : false;
        // ALERT
        if (!passed) {
            alert('One or more nextYrsSalary() tests failed.');
        }
    };
    test.yearsSavingsFromSalary = function () {
        var curSlry = 35000,
            expected1Yr = 2100.00,
            expected2Yr = 2179.80,
            expected16Yr = 3674.34,
            result1Yr = clc.yearsSavingsFromSalary(utl.percentToDecimal(6), curSlry),
            result2Yr = clc.yearsSavingsFromSalary(utl.percentToDecimal(6), 36330.00),
            result16Yr = clc.yearsSavingsFromSalary(utl.percentToDecimal(6), 61239.04),
            passed1yr = parseFloat(utl.roundToHundreds(result1Yr)) === parseFloat(expected1Yr),
            passed2yr = parseFloat(utl.roundToHundreds(result2Yr)) === parseFloat(expected2Yr),
            passed16yr = parseFloat(utl.roundToHundreds(result16Yr)) === parseFloat(expected16Yr),
            passed = (passed1yr && passed2yr && passed16yr) ? true : false;
        if (!passed)
            alert('One or more yearsSavingsFromSalary() tests failed.');
    };
    test.allFinalOutputsForAge30Retire67 = function (showValsInConsole) {
        var msg;
        if ($scope.calculatorForm.$dirty) {
            msg = '------------------------- \n';
            msg += '  TEST NOTE: "test.allFinalOutputsForAge30Retire67()" validates that INITIAL\n';
            msg += '  DEFAULT form values compute to match "PROOF.XLS > AGE 30 RETIRE 67". \n';
            msg += '  The form values have changed since page load so this test was not run.\n';
            msg += '------------------------- \n';
            //console.log(msg);
            return;
        }

        showValsInConsole = utl.defaultFor(showValsInConsole, false);
        if ($scope.inputs.ageNow !== 30)
            alert('$scope.inputs.ageNow = ' + $scope.inputs.ageNow + ', expected 30');
        if ($scope.inputs.ageRetire !== 67)
            alert('$scope.inputs.ageRetire = ' + $scope.inputs.ageRetire + ', expected 67');
        if ($scope.inputs.getDateOfRetire() !== 2051)
            alert('$scope.inputs.getDateOfRetire() = ' + $scope.inputs.getDateOfRetire() + ', expected 2051');
        if ($scope.inputs.salary !== 260000)
            alert('$scope.inputs.salary = ' + $scope.inputs.salary + ', expected 260000');
        if ($scope.inputs.getCountOfWorkYears() !== 37)
            alert('$scope.inputs.getCountOfWorkYears() = ' + $scope.inputs.getCountOfWorkYears() + ', expected 37');
        if ($scope.inputs.savingsBase !== 5000)
            alert('$scope.inputs.savingsBase = ' + $scope.inputs.savingsBase + ', expected 5000');
        if ($scope.inputs.savingsRateAsPercent !== 1)
            alert('$scope.inputs.savingsRateAsPercent = ' + $scope.inputs.savingsRateAsPercent + ', expected 1');
        // ASSUMPTIONS
        if ($scope.asm.inf !== 0.023)
            alert('$scope.asm.inf = ' + $scope.asm.inf + ', expected 0.023');
        if ($scope.asm.wgGrw !== 0.015)
            alert('$scope.asm.wgGrw = ' + $scope.asm.wgGrw + ', expected 0.015');
        if ($scope.asm.rateOr !== 0.06)
            alert('$scope.asm.rateOr = ' + $scope.asm.rateOr + ', expected 0.06');
        if ($scope.asm.repRate !== 0.85)
            alert('$scope.asm.repRate = ' + $scope.asm.repRate + ', expected 0.85');
        if ($scope.asm.eolAge !== 92)
            alert('$scope.asm.eolAge = ' + $scope.asm.eolAge + ', expected 92');
        // SS FACTORS USED
        if ($scope.ssf.factorsYr !== 2014)
            alert('$scope.ssf.factorsYr = ' + $scope.ssf.factorsYr + ', expected 2014');
        if ($scope.ssf.contribBase !== 117000)
            alert('$scope.ssf.contribBase = ' + $scope.ssf.contribBase + ', expected 117000');
        if ($scope.ssf.bptA !== 815.78)
            alert('$scope.ssf.bptA = ' + $scope.ssf.bptA + ', expected 815.78');
        if ($scope.ssf.bptB !== 4917.36)
            alert('$scope.ssf.bptB = ' + $scope.ssf.bptB + ', expected 4917.36');
        // CALCULATED RETIREMENT FACTORS
        if ($scope.output.aime !== 30574.013480657482)
            alert('$scope.output.aime = ' + $scope.output.aime + ', expected 30574.013480657482');
        if ($scope.output.pia !== 8904.2)
            alert('$scope.output.pia = ' + $scope.output.pia + ', expected 8904.2');
        if ($scope.output.bptA !== 2691)
            alert('$scope.output.bptA = ' + $scope.output.bptA + ', expected 2691');
        if ($scope.output.bptB !== 16220)
            alert('$scope.output.bptB = ' + $scope.output.bptB + ', expected 16220');
        if ($scope.output.ssMnthBenAt67 !== 9976)
            alert('$scope.output.ssMnthBenAt67 = ' + $scope.output.ssMnthBenAt67 + ', expected 9976');
        if ($scope.output.totalSvngsOnRtrmnt !== 594000)
            alert('$scope.output.totalSvngsOnRtrmnt = ' + $scope.output.totalSvngsOnRtrmnt + ', expected 594000');
        if ($scope.output.totalOfYrlDiscntValsAtEol !== 14954731.930803044)
            alert('$scope.output.totalOfYrlDiscntValsAtEol = ' + $scope.output.totalOfYrlDiscntValsAtEol + ', expected 14954731.930803044');
        // UPON RETIREMENT
        if ($scope.output.willHave !== 594000)
            alert('$scope.output.willHave = ' + $scope.output.willHave + ', expected 594000');
        if ($scope.output.willNeed !== 12887000)
            alert('$scope.output.willNeed = ' + $scope.output.willNeed + ', expected 12887000');
        if ($scope.output.getSurplus() !== -12293000)
            alert('$scope.output.getSurplus() = ' + $scope.output.getSurplus() + ', expected -12293000');
        if ($scope.output.willHaveTdyDlr !== 256000)
            alert('$scope.output.willHaveTdyDlr = ' + $scope.output.willHaveTdyDlr + ', expected 256000');
        if ($scope.output.willNeedTdyDlr !== 5556000)
            alert('$scope.output.willNeedTdyDlr = ' + $scope.output.willNeedTdyDlr + ', expected 5556000');
        if ($scope.output.getSurplusTdyDlr() !== -5300000)
            alert('$scope.output.getSurplusTdyDlr() = ' + $scope.output.getSurplusTdyDlr() + ', expected -5300000');
        // Print values to console?
        if (showValsInConsole) {
            /!** console.log('\nCurrent Age: ' + $scope.inputs.ageNow
                    + '\nRetirement Age: ' + $scope.inputs.ageRetire
                    + '\nRetirement Date: ' + $scope.inputs.getDateOfRetire()
                    + '\nCurrent Salary: ' + $scope.inputs.salary
                    + '\nSalaried Yrs Left: ' + $scope.inputs.getCountOfWorkYears()
                    + '\nCurrent Savings: ' + $scope.inputs.savingsBase
                    + '\nSavings Rate: ' + $scope.inputs.savingsRateAsPercent
                    // ASSUMPTIONS
                    + '\nInflation: ' + $scope.asm.inf
                    + '\nWage Growth: ' + $scope.asm.wgGrw
                    + '\nRate Of Return: ' + $scope.asm.rateOr
                    + '\nReplacement Rate: ' + $scope.asm.repRate
                    + '\nSurvive To (STA): ' + $scope.asm.eolAge
                    // SS FACTORS USED
                    + '\nSS Factors For Year: ' + $scope.ssf.factorsYr
                    + '\nContribution Base: ' + $scope.ssf.contribBase
                    + '\nBend Points A: ' + $scope.ssf.bptA
                    + '\nBend Points B: ' + $scope.ssf.bptB
                    // CALCULATED RETIREMENT FACTORS
                    + '\nAIM: ' + $scope.output.aime
                    + '\nPIA: ' + $scope.output.pia
                    + '\nBend PointA at 62: ' + $scope.output.bptA
                    + '\nBend PointB at 62: ' + $scope.output.bptB
                    + '\nSS Mnth Benif. at 67: ' + $scope.output.ssMnthBenAt67
                    + '\nTotal Svngs Upon Rtrmnt: ' + $scope.output.totalSvngsOnRtrmnt
                    + '\nTotal Yrly Discnt Vals At STA: ' + $scope.output.totalOfYrlDiscntValsAtEol
                    // UPON RETIREMENT
                    + '\nWill have in tmrw dollars : ' + $scope.output.willHave
                    + '\nWill need: ' + $scope.output.willNeed
                    + '\nDifference: ' + $scope.output.getSurplus()
                    + '\nWill have in tdy dollars: ' + $scope.output.willHaveTdyDlr
                    + '\nWill need: ' + $scope.output.willNeedTdyDlr
                    + '\nDifference: ' + $scope.output.getSurplusTdyDlr()
                  );
                  **!/
        } // endif
    };
}
]);
*/
