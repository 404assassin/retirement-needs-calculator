import { TestBed } from '@angular/core/testing';

import { DynamicVariablesService } from './dynamic-variables.service';

describe('DynamicVariablesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicVariablesService = TestBed.get(DynamicVariablesService);
    expect(service).toBeTruthy();
  });
});
