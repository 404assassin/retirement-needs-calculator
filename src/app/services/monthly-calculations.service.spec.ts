import { TestBed } from '@angular/core/testing';

import { MonthlyCalculationsService } from './monthly-calculations.service';

describe('MonthlyCalculationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MonthlyCalculationsService = TestBed.get(MonthlyCalculationsService);
    expect(service).toBeTruthy();
  });
});
